﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orangebox.ShutdownTimer.BusinessLayer.Logic;
using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;

namespace Orangebox.ShutdownTimer.BusinessLayer.Tests.Logic
{
    [TestClass]
    public class SpaValidatorTests
    {
        private readonly PowerAction SAMPLE_PWR_ACTION = new PowerAction("sample", "sample", "sample", "sample", false);
        private SpaValidator spaValidator;

        #region Setup
        [TestInitialize]
        public void BeforeEach()
        {
            spaValidator = new SpaValidator();
        }
        #endregion

        #region Private methods
        private ScheduledPowerAction GetSpa(TimeSpan timeToAction)
        {
            var spa = new ScheduledPowerAction(SAMPLE_PWR_ACTION, timeToAction);
            Console.WriteLine($"LOWER: {DateTime.Now.Add(SpaValidator.LOWER_LIMIT)}");
            Console.WriteLine($"UPPER: {DateTime.Now.Add(SpaValidator.UPPER_LIMIT)}");
            Console.WriteLine($"TIME OF ACTION: {spa.TimeOfAction}");
            return spa;
        }

        private ScheduledPowerAction GetSpa(DateTime timeOfAction)
        {
            var spa = new ScheduledPowerAction(SAMPLE_PWR_ACTION, timeOfAction);
            Console.WriteLine($"LOWER: {DateTime.Now.Add(SpaValidator.LOWER_LIMIT)}");
            Console.WriteLine($"UPPER: {DateTime.Now.Add(SpaValidator.UPPER_LIMIT)}");
            Console.WriteLine($"TIME OF ACTION: {spa.TimeOfAction}");
            return spa;
        }
        #endregion

        #region When Validating
        [TestMethod]
        public void ValidateShouldPassIfEqualToLower()
        {
            //Adding 5 ms because this test will randomly fail without it
            var spa = GetSpa(SpaValidator.LOWER_LIMIT.Add(TimeSpan.FromMilliseconds(5.0)));

            var result = spaValidator.Validate(spa);
            if (!result.IsSuccessful)
                Console.WriteLine($"MESSAGE: {result.ErrorMessage}");

            result.IsSuccessful.Should().BeTrue();
            result.ErrorMessage.Should().BeNullOrEmpty();
        }

        [TestMethod]
        public void ValidateShouldPassIfGreaterThanLower()
        {
            var spa = GetSpa(SpaValidator.LOWER_LIMIT.Add(TimeSpan.FromSeconds(1.0)));

            var result = spaValidator.Validate(spa);
            if (!result.IsSuccessful)
                Console.WriteLine($"MESSAGE: {result.ErrorMessage}");

            result.IsSuccessful.Should().BeTrue();
            result.ErrorMessage.Should().BeNullOrEmpty();
        }

        [TestMethod]
        public void ValidateShouldPassIfEqualToUpper()
        {
            var spa = GetSpa(SpaValidator.UPPER_LIMIT);

            var result = spaValidator.Validate(spa);
            if (!result.IsSuccessful)
                Console.WriteLine($"MESSAGE: {result.ErrorMessage}");

            result.IsSuccessful.Should().BeTrue();
            result.ErrorMessage.Should().BeNullOrEmpty();
        }

        [TestMethod]
        public void ValidateShouldPassIfLessThanUpper()
        {
            var spa = GetSpa(SpaValidator.UPPER_LIMIT.Subtract(TimeSpan.FromSeconds(1.0)));

            var result = spaValidator.Validate(spa);
            if (!result.IsSuccessful)
                Console.WriteLine($"MESSAGE: {result.ErrorMessage}");

            result.IsSuccessful.Should().BeTrue();
            result.ErrorMessage.Should().BeNullOrEmpty();
        }

        [TestMethod]
        public void ValidateShouldFailIfLessThanLower()
        {
            var spa = GetSpa(SpaValidator.LOWER_LIMIT.Subtract(TimeSpan.FromSeconds(1.0)));

            var result = spaValidator.Validate(spa);
            if (!result.IsSuccessful)
                Console.WriteLine($"MESSAGE: {result.ErrorMessage}");

            result.IsSuccessful.Should().BeFalse();
            result.ErrorMessage.Should().NotBeNullOrEmpty();
        }

        [TestMethod]
        public void ValidateShouldFailIfGreaterThanUpper()
        {
            var spa = GetSpa(SpaValidator.UPPER_LIMIT.Add(TimeSpan.FromSeconds(1.0)));

            var result = spaValidator.Validate(spa);
            if (!result.IsSuccessful)
                Console.WriteLine($"MESSAGE: {result.ErrorMessage}");

            result.IsSuccessful.Should().BeFalse();
            result.ErrorMessage.Should().NotBeNullOrEmpty();
        }
        #endregion
    }
}
