﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Orangebox.Common.Xaml.Wpf.Mvvm.Dialog;
using Orangebox.ShutdownTimer.BusinessLayer.Logic;
using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;

namespace Orangebox.ShutdownTimer.BusinessLayer.Tests.Logic
{
    [TestClass]
    public class CloseHandlerTests
    {
        #region Variables
        private IDialogService dialogService;
        private IPowerActionExecutor powerActionExecutor;
        private CloseHandler closeHandler;
        #endregion

        #region Setup
        [TestInitialize]
        public void BeforeEach()
        {
            dialogService = Substitute.For<IDialogService>();
            powerActionExecutor = Substitute.For<IPowerActionExecutor>();
            closeHandler = new CloseHandler(dialogService, powerActionExecutor);
        }
        #endregion

        #region Private methods
        private ScheduledPowerAction GetSpa(bool isSchedulable = false)
        {
            var pa = new PowerAction("", "", "", "", isSchedulable);
            return new ScheduledPowerAction(pa, DateTime.Now.AddMinutes(5.0));
        }
        #endregion

        #region When Checking Can Close
        [TestMethod]
        public void CheckCanCloseShouldHaltIfCountdownNotRunning()
        {
            var spa = GetSpa();

            var canClose = closeHandler.CheckCanClose(false, spa);

            canClose.Should().BeTrue();
            dialogService.DidNotReceive().ShowYesNo(Arg.Any<String>(), Arg.Any<String>());
            dialogService.DidNotReceive().ShowOkCancel(Arg.Any<String>(), Arg.Any<String>());
            powerActionExecutor.DidNotReceive().Run(Arg.Any<PowerAction>());
            powerActionExecutor.DidNotReceive().Run(Arg.Any<PowerAction>(), Arg.Any<Int32>());
        }

        [TestMethod]
        public void CheckCanCloseShouldHaltIfSpaIsNull()
        {
            var canClose = closeHandler.CheckCanClose(true, null);

            canClose.Should().BeTrue();
            dialogService.DidNotReceive().ShowYesNo(Arg.Any<String>(), Arg.Any<String>());
            dialogService.DidNotReceive().ShowOkCancel(Arg.Any<String>(), Arg.Any<String>());
            powerActionExecutor.DidNotReceive().Run(Arg.Any<PowerAction>());
            powerActionExecutor.DidNotReceive().Run(Arg.Any<PowerAction>(), Arg.Any<Int32>());
        }

        [TestMethod]
        public void CheckCanCloseShouldReturnFalseIfUserClicksCancel()
        {
            var spa = GetSpa(true);
            dialogService.ShowYesNoCancel(Arg.Any<String>(), Arg.Any<String>()).Returns((bool?)null);

            var canClose = closeHandler.CheckCanClose(true, spa);

            dialogService.Received().ShowYesNoCancel(Arg.Any<String>(), Arg.Any<String>());
            canClose.Should().BeFalse();
        }

        [TestMethod]
        public void CheckCanCloseShouldSubmitSpaIfUserClicksYes()
        {
            var spa = GetSpa(true);
            dialogService.ShowYesNoCancel(Arg.Any<String>(), Arg.Any<String>()).Returns(true);

            var canClose = closeHandler.CheckCanClose(true, spa);

            powerActionExecutor.Received().Run(spa.PowerAction, Arg.Any<Int32>());
            canClose.Should().BeTrue();
        }

        [TestMethod]
        public void CheckCanCloseShouldNotSubmitSpaIfUserClicksNo()
        {
            var spa = GetSpa(true);
            dialogService.ShowYesNoCancel(Arg.Any<String>(), Arg.Any<String>()).Returns(false);

            var canClose = closeHandler.CheckCanClose(true, spa);

            powerActionExecutor.DidNotReceive().Run(spa.PowerAction, (Int32)(spa.TimeOfAction - DateTime.Now).TotalSeconds);
            canClose.Should().BeTrue();
        }

        [TestMethod]
        public void CheckCanCloseShouldWarnUserIfSpaIsNotSchedulable()
        {
            var spa = GetSpa(false);

            closeHandler.CheckCanClose(true, spa);

            dialogService.Received().ShowOkCancel(Arg.Any<String>(), Arg.Any<String>());
        }

        [TestMethod]
        public void CheckCanCloseShouldReturnUserResponseOnWarning()
        {
            var spa = GetSpa(false);
            var response = !default(bool);
            dialogService.ShowOkCancel(Arg.Any<String>(), Arg.Any<String>()).Returns(response);

            var canClose = closeHandler.CheckCanClose(true, spa);

            canClose.ShouldBeEquivalentTo(response);
        }
        #endregion
    }
}
