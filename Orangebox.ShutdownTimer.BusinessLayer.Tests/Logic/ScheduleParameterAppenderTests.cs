﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orangebox.ShutdownTimer.BusinessLayer.Logic;
using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orangebox.ShutdownTimer.BusinessLayer.Tests.Logic
{
    [TestClass]
    public class ScheduleParameterAppenderTests
    {
        #region Variables
        private ScheduleParameterAppender scheduleParameterAppender;
        #endregion

        #region Setup
        [TestInitialize]
        public void BeforeEach()
        {
            scheduleParameterAppender = new ScheduleParameterAppender();
        }
        #endregion

        #region When Appending Schedule To String
        [TestMethod]
        public void AppendScheduleStringShouldAppendTimeArgument()
        {
            var parameters = "sample params";
            var timeInSeconds = 0;

            var actualParameters = scheduleParameterAppender.AppendSchedule(parameters, timeInSeconds);

            actualParameters.ShouldBeEquivalentTo($"{parameters} /t {timeInSeconds}");
        }

        [TestMethod]
        public void AppendScheduleStringShouldThrowExceptionOnEmptyParameterString()
        {
            var action = new Action(() => scheduleParameterAppender.AppendSchedule(String.Empty, 0));

            action.ShouldThrow<ArgumentNullException>();
        }

        [TestMethod]
        public void AppendScheduleStringShouldThrowExceptionOnNullParameterString()
        {
            var action = new Action(() => 
                scheduleParameterAppender.AppendSchedule(parameters: null, waitTimeInSeconds: 0)
            );

            action.ShouldThrow<ArgumentNullException>();
        }
        #endregion

        #region When Appending Schedule To PowerAction
        [TestMethod]
        public void AppendSchedulePowerActionShouldReturnParametersIfNotSchedulable()
        {
            var powerAction = new PowerAction("asdf", "asdf", "asdf", "asdf", false);

            var actualParameters = scheduleParameterAppender.AppendSchedule(powerAction, 0);

            actualParameters.ShouldBeEquivalentTo(powerAction.Parameters);
        }

        [TestMethod]
        public void AppendSchedulePowerActionShouldAppendTimeArgumentIfSchedulable()
        {
            var powerAction = new PowerAction("asdf", "asdf", "asdf", "asdf", true);
            var timeInSeconds = 0;

            var actualParameters = scheduleParameterAppender.AppendSchedule(powerAction, timeInSeconds);

            actualParameters.ShouldBeEquivalentTo($"{powerAction.Parameters} /t {timeInSeconds}");

        }
        #endregion
    }
}
