﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Orangebox.Common.Helpers;
using Orangebox.ShutdownTimer.BusinessLayer.Logic;
using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;
using System.Collections.Generic;

namespace Orangebox.ShutdownTimer.BusinessLayer.Tests.Logic
{
    [TestClass]
    public class ExceptionCopierTests
    {
        #region Variables
        private IClipboardHelper clipboardHelper;
        private ExceptionCopier exceptionCopier;
        #endregion

        #region Setup
        [TestInitialize]
        public void BeforeEach()
        {
            clipboardHelper = Substitute.For<IClipboardHelper>();
            exceptionCopier = new ExceptionCopier(clipboardHelper);
        }
        #endregion

        #region Private Methods
        private ExceptionModel GetExceptionModel()
        {
            return new ExceptionModel(typeof(NotImplementedException), "Sample", "Sample");
        }

        private List<ExceptionModel> GetExceptionModels()
        {
            var exceptions = new List<ExceptionModel>();
            for (var index = 0; index < 5; index++)
            {
                exceptions.Add(GetExceptionModel());
            }
            return exceptions;
        }
        #endregion

        #region When Copying Single Exception
        [TestMethod]
        public void CopyExceptionShouldCallClipboardHelper()
        {
            var exception = GetExceptionModel();

            exceptionCopier.CopyException(exception);

            clipboardHelper.Received(1).Copy(Arg.Any<String>());
        }
        #endregion

        #region When Copying Multiple Exceptions
        [TestMethod]
        public void CopyExceptionsShouldCallClipboardHelperOnce()
        {
            var exceptions = GetExceptionModels();

            exceptionCopier.CopyExceptions(exceptions);

            clipboardHelper.Received(1).Copy(Arg.Any<String>());
        }
        #endregion
    }
}
