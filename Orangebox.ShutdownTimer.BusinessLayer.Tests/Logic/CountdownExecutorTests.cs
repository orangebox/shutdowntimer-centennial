﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orangebox.ShutdownTimer.BusinessLayer.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orangebox.ShutdownTimer.BusinessLayer.Tests.Logic
{
    [TestClass]
    public class CountdownExecutorTests
    {
        #region Variables
        private CountdownExecutor countdownExecutor;
        #endregion

        #region Setup
        [TestInitialize]
        public void BeforeEach()
        {
            countdownExecutor = new CountdownExecutor();
        }
        #endregion

        #region When Running Timer
        [TestMethod]
        public async Task RunTimerShouldSetIsRunningFlag()
        {
            await countdownExecutor.RunTimer(DateTime.Now.AddSeconds(1.0), (ts) => { return; });

            countdownExecutor.IsRunning.Should().BeTrue();
        }

        [TestMethod]
        public async Task RunTimerShouldDecreaseRemainingTime()
        {
            var waitTime = new TimeSpan(0, 0, 3);

            await countdownExecutor.RunTimer(DateTime.Now.Add(waitTime), (ts) => waitTime = ts);

            waitTime.Should().BeLessOrEqualTo(TimeSpan.Zero);
        }

        [TestMethod]
        public async Task RunTimerShouldReturnCountdownResultWhenNotInterrupted()
        {
            var result = await countdownExecutor.RunTimer(DateTime.Now.AddSeconds(1.0), (ts) => { return; });

            result.IsInterrupted.Should().BeFalse();
        }

        [TestMethod]
        public async Task RunTimerShouldReturnCountdownResultWhenInterrupted()
        {
            var waitTime = new TimeSpan(0, 0, 3);
            var halfWaitTimeMs = waitTime.TotalMilliseconds / 2.0;
            var setIsRunningAsync = Task.Run(async () =>
            {
                await Task.Delay((int)halfWaitTimeMs);
                countdownExecutor.IsRunning = false;
            });

            var result = await countdownExecutor.RunTimer(DateTime.Now.Add(waitTime), (ts) => { return; });

            result.IsInterrupted.Should().BeTrue();
        }
        #endregion
    }
}
