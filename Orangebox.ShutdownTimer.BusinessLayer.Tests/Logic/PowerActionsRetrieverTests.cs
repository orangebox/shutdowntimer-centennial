﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orangebox.ShutdownTimer.BusinessLayer.Logic;
using System.Linq;

namespace Orangebox.ShutdownTimer.BusinessLayer.Tests.Logic
{
    [TestClass]
    public class PowerActionsRetrieverTests
    {
        #region Variables
        private PowerActionsRetriever powerActionsRetriever;
        #endregion

        #region Setup
        [TestInitialize]
        public void BeforeEach()
        {
            powerActionsRetriever = new PowerActionsRetriever();
        }
        #endregion

        #region When Retrieving Actions
        [TestMethod]
        public void WhenRetrievingActionsShouldReturnDistinctActions()
        {
            var retrievedActions = powerActionsRetriever.GetPowerActions();
            var distinctActions = retrievedActions.Distinct();

            retrievedActions.Should().NotBeEmpty();
            distinctActions.Should().HaveSameCount(retrievedActions);
        }
        #endregion
    }
}
