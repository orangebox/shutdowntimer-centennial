﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Orangebox.Common.Helpers;
using Orangebox.ShutdownTimer.BusinessLayer.Logic;
using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;

namespace Orangebox.ShutdownTimer.BusinessLayer.Tests.Logic
{
    [TestClass]
    public class PowerActionExecutorTests
    {
        #region Variables
        private IProcessExecutor processExecutor;
        private IScheduleParameterAppender scheduleParameterAppender;
        private PowerActionExecutor powerActionExecutor;
        #endregion

        #region Setup
        [TestInitialize]
        public void BeforeEach()
        {
            processExecutor = Substitute.For<IProcessExecutor>();
            scheduleParameterAppender = Substitute.For<IScheduleParameterAppender>();
            powerActionExecutor = new PowerActionExecutor(processExecutor, scheduleParameterAppender);
        }
        #endregion

        #region Private Methods
        private PowerAction GetPowerAction()
        {
            return new PowerAction("name", "filename", "parameters", "description", false);
        }
        #endregion

        #region When Running Without Time Argument
        [TestMethod]
        public void RunShouldCallProcessExecutor()
        {
            var powerAction = GetPowerAction();
            scheduleParameterAppender.AppendSchedule(powerAction, 0).Returns(powerAction.Parameters);

            powerActionExecutor.Run(powerAction);

            processExecutor.Received().Run(powerAction.Filename, powerAction.Parameters);
        }

        [TestMethod]
        public void RunShouldReturnResultWithExitCode()
        {
            var exitCode = 71687;
            processExecutor.Run(Arg.Any<String>(), Arg.Any<String>()).Returns(exitCode);

            var powerActionResult = powerActionExecutor.Run(new PowerAction("", "", "", "", false));

            powerActionResult.ExitCode.ShouldBeEquivalentTo(exitCode);
        }

        [TestMethod]
        public void RunShouldCallScheduleParameterAppender()
        {
            var powerAction = GetPowerAction();

            powerActionExecutor.Run(powerAction);

            scheduleParameterAppender.Received().AppendSchedule(powerAction, 0);
        }
        #endregion

        #region When Running With Time Argument
        [TestMethod]
        public void RunShouldPassTimeArgumentToScheduleParameterAppender()
        {
            var powerAction = GetPowerAction();
            var waitTimeInSeconds = Int32.MaxValue;

            powerActionExecutor.Run(powerAction, waitTimeInSeconds);

            scheduleParameterAppender.Received().AppendSchedule(powerAction, waitTimeInSeconds);
        }

        [TestMethod]
        public void RunWithTimeArgumentShouldCallProcessExecutor()
        {
            var powerAction = GetPowerAction();
            var waitTimeInSeconds = Int32.MaxValue;
            scheduleParameterAppender.AppendSchedule(powerAction, waitTimeInSeconds)
                                     .Returns(powerAction.Parameters);

            powerActionExecutor.Run(powerAction, waitTimeInSeconds);

            processExecutor.Received().Run(powerAction.Filename, powerAction.Parameters);
        }

        [TestMethod]
        public void RunWithTimeArgumentShouldReturnResultWithExitCode()
        {
            var exitCode = 21089;
            var waitTimeInSeconds = Int32.MaxValue;
            var powerAction = GetPowerAction();
            scheduleParameterAppender.AppendSchedule(powerAction, waitTimeInSeconds)
                                     .Returns(powerAction.Parameters);
            processExecutor.Run(powerAction.Filename, powerAction.Parameters).Returns(exitCode);

            var powerActionResult = powerActionExecutor.Run(powerAction, waitTimeInSeconds);

            powerActionResult.ExitCode.ShouldBeEquivalentTo(exitCode);
        }
        #endregion
    }

    [TestClass]
    public class DebugPowerActionExecutorTests
    {
        #region Variables
        private IProcessExecutor processExecutor;
        private DebugPowerActionExecutor debugPowerActionExecutor;
        #endregion

        #region Setup
        [TestInitialize]
        public void BeforeEach()
        {
            processExecutor = Substitute.For<IProcessExecutor>();
            debugPowerActionExecutor = new DebugPowerActionExecutor(processExecutor);
        }
        #endregion

        #region When Running
        [TestMethod]
        public void RunShouldSetShutdownActionAndImmediatelyAbortIt()
        {
            debugPowerActionExecutor.Run(new PowerAction(String.Empty, String.Empty, String.Empty, String.Empty, false));

            processExecutor.Received().Run("shutdown.exe", "/s /t 3600");
            processExecutor.Received().Run("shutdown.exe", "/a");
        }
        #endregion
    }
}
