﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orangebox.ShutdownTimer.BusinessLayer.Models;

namespace Orangebox.ShutdownTimer.BusinessLayer.Tests.Models
{
    [TestClass]
    public class CountdownResultTests
    {
        #region When Initializing
        [TestMethod]
        public void CtorShouldSetIsInterruptedToTrue()
        {
            var isInterrupted = true;

            var countdownResult = new CountdownResult(isInterrupted);

            countdownResult.IsInterrupted.ShouldBeEquivalentTo(isInterrupted);
        }

        [TestMethod]
        public void CtorShouldSetIsInterruptedToFalse()
        {
            var isInterrupted = false;

            var countdownResult = new CountdownResult(isInterrupted);

            countdownResult.IsInterrupted.ShouldBeEquivalentTo(isInterrupted);
        }
        #endregion
    }
}
