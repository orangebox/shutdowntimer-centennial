﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orangebox.ShutdownTimer.BusinessLayer.Models;

namespace Orangebox.ShutdownTimer.BusinessLayer.Tests.Models
{
    [TestClass]
    public class PowerActionTests
    {
        #region When Initializing
        [TestMethod]
        public void CtorShouldSetName()
        {
            var name = "name";

            var pa = new PowerAction(name, "asdf", "asdf", "asdf", false);

            pa.Name.ShouldBeEquivalentTo(name);
        }

        [TestMethod]
        public void CtorShouldSetFilename()
        {
            var filename = "filename";

            var pa = new PowerAction("asdf", filename, "asdf", "asdf", false);

            pa.Filename.ShouldBeEquivalentTo(filename);
        }

        [TestMethod]
        public void CtorShouldSetArgument()
        {
            var argument = "argument";

            var pa = new PowerAction("asdf", "asdf", argument, "asdf", false);

            pa.Parameters.ShouldBeEquivalentTo(argument);
        }

        [TestMethod]
        public void CtorShouldSetDescription()
        {
            var description = "description";

            var pa = new PowerAction("asdf", "asdf", "asdf", description, false);

            pa.Description.ShouldBeEquivalentTo(description);
        }

        [TestMethod]
        public void CtorShouldSetIsSchedulable()
        {
            var isSchedulable = !default(bool);

            var pa = new PowerAction("asdf", "asdf", "asdf", "asdf", isSchedulable);

            pa.IsSchedulable.ShouldBeEquivalentTo(isSchedulable);
        }
        #endregion
    }
}
