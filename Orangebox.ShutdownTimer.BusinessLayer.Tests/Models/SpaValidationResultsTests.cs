﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;

namespace Orangebox.ShutdownTimer.BusinessLayer.Tests.Models
{
    [TestClass]
    public class SpaValidationResultsTests
    {
        [TestMethod]
        public void CtorShouldSetMessage()
        {
            var message = "this is a message";

            var spaResult = new SpaValidationResult(message);

            spaResult.ErrorMessage.ShouldBeEquivalentTo(message);
        }

        [TestMethod]
        public void IsSuccessfulShouldBeFalseWhenMessageHasText()
        {
            var spaResult = new SpaValidationResult("sample");
            spaResult.IsSuccessful.Should().BeFalse();
        }

        [TestMethod]
        public void IsSuccessfulShouldBeTrueWhenMessageIsEmpty()
        {
            var spaResult = new SpaValidationResult(String.Empty);
            spaResult.IsSuccessful.Should().BeTrue();
        }

        [TestMethod]
        public void IsSuccessfulShouldBeTrueWhenMessageIsNull()
        {
            var spaResult = new SpaValidationResult(null);
            spaResult.IsSuccessful.Should().BeTrue();
        }

    }
}
