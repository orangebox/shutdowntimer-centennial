﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;

namespace Orangebox.ShutdownTimer.BusinessLayer.Tests.Models
{
    [TestClass]
    public class ExceptionModelTests
    {
        [TestMethod]
        public void CtorHandlesExceptionParameter()
        {
            try
            {
                throw new NotImplementedException("sample");
            }
            catch (Exception e)
            {
                var exceptionModel = new ExceptionModel(e);
                exceptionModel.Type.ShouldBeEquivalentTo(e.GetType().Name);
                exceptionModel.Message.ShouldBeEquivalentTo(e.Message);
                exceptionModel.StackTrace.ShouldBeEquivalentTo(e.StackTrace);
            }
        }

        [TestMethod]
        public void CtorHandlesTypeParameter()
        {
            try
            {
                throw new NotImplementedException("sample");
            }
            catch (Exception e)
            {
                var exceptionModel = new ExceptionModel(e.GetType(), e.Message, e.StackTrace);
                exceptionModel.Type.ShouldBeEquivalentTo(e.GetType().Name);
                exceptionModel.Message.ShouldBeEquivalentTo(e.Message);
                exceptionModel.StackTrace.ShouldBeEquivalentTo(e.StackTrace);
            }
        }

        [TestMethod]
        public void CtorHandlesExplicitParameters()
        {
            try
            {
                throw new NotImplementedException("sample");
            }
            catch (Exception e)
            {
                var exceptionModel = new ExceptionModel(e.GetType().Name, e.Message, e.StackTrace);
                exceptionModel.Type.ShouldBeEquivalentTo(e.GetType().Name);
                exceptionModel.Message.ShouldBeEquivalentTo(e.Message);
                exceptionModel.StackTrace.ShouldBeEquivalentTo(e.StackTrace);
            }
        }

    }
}
