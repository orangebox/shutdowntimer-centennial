﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;

namespace Orangebox.ShutdownTimer.BusinessLayer.Tests.Models
{
    [TestClass]
    public class ScheduledPowerActionTests
    {
        private readonly PowerAction POWER_ACTION_1 = new PowerAction("sample1", "sample1", "sample1", "sample1", false);
        private readonly PowerAction POWER_ACTION_2 = new PowerAction("sample2", "sample2", "sample2", "sample2", false);
        private readonly DateTime DATE_1 = new DateTime(1987, 7, 16, 0, 30, 0);
        private readonly DateTime DATE_2 = new DateTime(1989, 2, 10, 0, 0, 0);

        #region Setup
        [ClassInitialize]
        public static void BeforeAll(TestContext tc)
        {

        }

        [TestInitialize]
        public void BeforeEach()
        {
            
        }
        #endregion

        #region When Initializing
        [TestMethod]
        public void DateTimeCtorShouldSetPowerAction()
        {
            var spa = new ScheduledPowerAction(POWER_ACTION_1, DateTime.Now);
            spa.PowerAction.ShouldBeEquivalentTo(POWER_ACTION_1);
        }

        [TestMethod]
        public void DateTimeCtorShouldSetTimeOfAction()
        {
            var expectedTime = DateTime.Now;
            var spa = new ScheduledPowerAction(POWER_ACTION_1, expectedTime);
            spa.TimeOfAction.ShouldBeEquivalentTo(expectedTime);
        }

        [TestMethod]
        public void TimeSpanCtorShouldSetPowerAction()
        {
            var spa = new ScheduledPowerAction(POWER_ACTION_1, TimeSpan.FromDays(2.0));
            spa.PowerAction.ShouldBeEquivalentTo(POWER_ACTION_1);
        }

        [TestMethod]
        public void TimeSpanCtorShouldSetTimeOfAction()
        {
            var expectedTime = DateTime.Now.AddHours(1.0);
            var spa = new ScheduledPowerAction(POWER_ACTION_1, TimeSpan.FromHours(1.0));
            spa.TimeOfAction.ShouldBeEquivalentTo(expectedTime);
        }
        #endregion

        #region When Checking Equality
        [TestMethod]
        public void EqualsShouldPassOnSameParameters()
        {
            var spa1 = new ScheduledPowerAction(POWER_ACTION_1, DATE_1);
            var spa2 = new ScheduledPowerAction(POWER_ACTION_1, DATE_1);

            var areEqual = spa1.Equals(spa2);

            areEqual.Should().BeTrue();
            spa1.ShouldBeEquivalentTo(spa2);
        }

        [TestMethod]
        public void EqualsShouldFailOnDifferentDates()
        {
            var spa1 = new ScheduledPowerAction(POWER_ACTION_1, DATE_1);
            var spa2 = new ScheduledPowerAction(POWER_ACTION_1, DATE_2);

            var areEqual = spa1.Equals(spa2);

            areEqual.Should().BeFalse();
            spa1.Should().NotBe(spa2);
        }

        [TestMethod]
        public void EqualsShouldFailOnDifferentPowerActions()
        {
            var spa1 = new ScheduledPowerAction(POWER_ACTION_1, DATE_1);
            var spa2 = new ScheduledPowerAction(POWER_ACTION_2, DATE_1);

            var areEqual = spa1.Equals(spa2);

            areEqual.Should().BeFalse();
            spa1.Should().NotBe(spa2);
        }

        [TestMethod]
        public void EqualsShouldFailOnNullInput()
        {
            var areEqual = (new ScheduledPowerAction(POWER_ACTION_1, DateTime.Now)).Equals(null);
            areEqual.Should().BeFalse();
        }

        [TestMethod]
        public void EqualsShouldFailOnDifferentTypes()
        {
            var areEqual = (new ScheduledPowerAction(POWER_ACTION_1, DateTime.Now)).Equals(123);
            areEqual.Should().BeFalse();
        }
        #endregion
    }
}
