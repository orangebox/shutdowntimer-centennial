﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orangebox.ShutdownTimer.BusinessLayer.Models;

namespace Orangebox.ShutdownTimer.BusinessLayer.Tests.Models
{
    [TestClass]
    public class PowerActionResultTests
    {
        #region When Initializing
        [TestMethod]
        public void CtorShouldSetExitCode()
        {
            var exitCode = 1234567890;

            var powerActionResult = new PowerActionResult(exitCode);

            powerActionResult.ExitCode.ShouldBeEquivalentTo(exitCode);
        }

        [TestMethod]
        public void ErrorMessageShouldHandleUnexpectedExitCodes()
        {
            var exitCode = PowerActionResult.NOTHING_SCHEDULED * PowerActionResult.ALREADY_SCHEDULED;

            var powerActionResult = new PowerActionResult(exitCode);

            powerActionResult.ErrorMessage.Should().Contain(exitCode.ToString());
        }

        [TestMethod]
        public void IsSuccessfulShouldBeTrueWhenExitCodeIsZero()
        {
            var powerActionResult = new PowerActionResult(PowerActionResult.SUCCESS);

            powerActionResult.IsSuccessful.Should().BeTrue();
        }

        [TestMethod]
        public void IsSuccessfulShouldBeFalseWhenExitCodeIsNotZero()
        {
            var powerActionResult = new PowerActionResult(PowerActionResult.BAD_PARAMS);

            powerActionResult.IsSuccessful.Should().BeFalse();
        }
        #endregion
    }
}
