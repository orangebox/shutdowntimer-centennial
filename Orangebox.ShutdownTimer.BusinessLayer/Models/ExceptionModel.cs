﻿using System;

namespace Orangebox.ShutdownTimer.BusinessLayer.Models
{
    public class ExceptionModel
    {
        #region Variables
        public string Type { get; }
        public string Message { get; }
        public string StackTrace { get; }
        #endregion

        #region Constructors
        public ExceptionModel(Exception ex)
            : this(ex.GetType().Name, ex.Message, ex.StackTrace)
        {

        }

        public ExceptionModel(Type type, string message, string stackTrace)
            : this(type.Name, message, stackTrace)
        {

        }

        public ExceptionModel(string type, string message, string stackTrace)
        {
            Type = type;
            Message = message;
            StackTrace = stackTrace;
        }
        #endregion
    }
}
