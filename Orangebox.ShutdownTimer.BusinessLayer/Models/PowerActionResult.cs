﻿using System;

namespace Orangebox.ShutdownTimer.BusinessLayer.Models
{
    public class PowerActionResult
    {
        #region Constants
        public const int SUCCESS = 0;
        public const int BAD_PARAMS = 1;
        public const int NOTHING_SCHEDULED = 1116;
        public const int ALREADY_SCHEDULED = 1190;
        #endregion

        #region Variables
        public int ExitCode { get; }
        public bool IsSuccessful { get { return ExitCode == SUCCESS; } }
        public string ErrorMessage
        {
            get
            {
                switch(ExitCode)
                {
                    case SUCCESS:
                        return String.Empty;
                    case BAD_PARAMS:
                        return "Invalid parameters";
                    case NOTHING_SCHEDULED:
                        return "Unable to abort the system shutdown because no shutdown was in progress.";
                    case ALREADY_SCHEDULED:
                        return "A system shutdown has already been scheduled.";
                    default:
                        return $"Unhandled exit code {ExitCode}.";
                }
            }
        }
        #endregion

        #region Constructors
        public PowerActionResult(int exitCode)
        {
            ExitCode = exitCode;
        }
        #endregion
    }
}
