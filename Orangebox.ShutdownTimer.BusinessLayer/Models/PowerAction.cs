﻿namespace Orangebox.ShutdownTimer.BusinessLayer.Models
{
    public class PowerAction
    {
        #region Variables
        /// <summary>The name of the action (e.g. "shut down", "restart", etc.)</summary>
        public string Name { get; }
        /// <summary>The name of the executable that runs the action</summary>
        public string Filename { get; }
        /// <summary>The parameters required to run the action</summary>
        public string Parameters { get; }
        /// <summary>Explains what the action is</summary>
        public string Description { get; }
        /// <summary>True if the command supports the "/t" argument</summary>
        public bool IsSchedulable { get; }
        #endregion

        #region Constructors
        public PowerAction(string name, 
                           string filename, 
                           string parameters, 
                           string description, 
                           bool isSchedulable)
        {
            Name = name;
            Filename = filename;
            Parameters = parameters;
            Description = description;
            IsSchedulable = isSchedulable;
        }
        #endregion
    }
}
