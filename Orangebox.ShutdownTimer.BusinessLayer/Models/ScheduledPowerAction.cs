﻿using System;

namespace Orangebox.ShutdownTimer.BusinessLayer.Models
{
    public class ScheduledPowerAction
    {
        #region Variables
        public DateTime TimeOfAction { get; private set; }
        public PowerAction PowerAction { get; private set; }
        #endregion

        #region Constructors
        public ScheduledPowerAction(PowerAction powerAction, DateTime timeOfAction)
        {
            PowerAction = powerAction;
            TimeOfAction = timeOfAction;
        }

        public ScheduledPowerAction(PowerAction powerAction, TimeSpan timeToAction)
            : this(powerAction, DateTime.Now.Add(timeToAction))
        {

        }
        #endregion

        #region Public methods
        public override int GetHashCode()
        {
            return this.TimeOfAction.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            else if (obj.GetType() != this.GetType())
                return false;
            else
            {
                var that = obj as ScheduledPowerAction;
                var isEqualName = this.PowerAction.Name == that.PowerAction.Name;
                var isEqualTime = this.TimeOfAction == that.TimeOfAction;
                return isEqualName && isEqualTime;
            }
        }
        #endregion

    }
}
