﻿namespace Orangebox.ShutdownTimer.BusinessLayer.Models
{
    public class CountdownResult
    {
        public bool IsInterrupted { get; }

        public CountdownResult(bool isInterrupted)
        {
            IsInterrupted = isInterrupted;
        }
    }

}
