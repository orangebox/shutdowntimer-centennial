﻿using System;

namespace Orangebox.ShutdownTimer.BusinessLayer.Models
{
    public class SpaValidationResult
    {
        public string ErrorMessage { get; }
        public bool IsSuccessful {  get { return String.IsNullOrEmpty(ErrorMessage); } }

        public SpaValidationResult(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }

    }
}
