﻿using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System.Collections.Generic;

namespace Orangebox.ShutdownTimer.BusinessLayer.Logic
{
    public interface IPowerActionsRetriever
    {
        PowerAction AbortAction { get; }
        IEnumerable<PowerAction> GetPowerActions();
    }

    public class PowerActionsRetriever : IPowerActionsRetriever
    {
        private readonly PowerAction SHUTDOWN = new PowerAction(
                "Shut down", 
                "shutdown.exe", 
                "/s /f", 
                "Shuts down the computer", 
                true
            );
        private readonly PowerAction RESTART = new PowerAction(
                "Restart", 
                "shutdown.exe", 
                "/r /f", 
                "Restarts the computer", 
                true
            );
        private readonly PowerAction RESTART_REOPEN = new PowerAction(
                "Restart and reopen apps", 
                "shutdown.exe", 
                "/g /f", 
                "Restarts the computer and reopens the registered apps", 
                true
            );
        private readonly PowerAction HIBERNATE = new PowerAction(
                "Hibernate", 
                "shutdown.exe", 
                "/h", 
                "Hibernates the computer", 
                false
            );
        private readonly PowerAction LOGOFF = new PowerAction(
                "Log off", 
                "shutdown.exe", 
                "/l", 
                "Logs the user off", 
                false
            );
        private readonly PowerAction HYBRID = new PowerAction(
                "Hybrid", 
                "shutdown.exe", 
                "/s /hybrid /f", 
                "Shuts the computer down and prepares it for fast startup", 
                true
            );
        private readonly PowerAction RESTART_ADVANCED = new PowerAction(
                "Open Advanced Boot Options", 
                "shutdown.exe", 
                "/r /o /f", 
                "Restarts the computer and loads the advanced boot options menu", 
                true
            );

        private readonly PowerAction SLEEP = new PowerAction(
                "Sleep", 
                "rundll32.exe", 
                "powrprof.dll,SetSuspendState 0,1,0", 
                "Puts the computer on standby", 
                false
            );
        private readonly PowerAction LOCK = new PowerAction(
                "Lock Workstation", 
                "rundll32.exe", 
                "User32.dll,LockWorkStation", 
                "Locks the computer, requiring a password to unlock", 
                false
            );

        public PowerAction AbortAction
        {
            get
            {
                return new PowerAction(
                        "Abort", 
                        "shutdown.exe", 
                        "/a", 
                        "Aborts any scheduled power actions", 
                        false
                    );
            }
        }

        public IEnumerable<PowerAction> GetPowerActions()
        {
            return new List<PowerAction>
            {
                SHUTDOWN,
                RESTART,
                SLEEP,
#if DEBUG
                RESTART_REOPEN,
                RESTART_ADVANCED,
#endif
                HIBERNATE,
                LOCK,
                LOGOFF,
#if DEBUG
                HYBRID
#endif
            };
        }

    }
}
