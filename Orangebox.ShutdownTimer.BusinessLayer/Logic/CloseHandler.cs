﻿using Orangebox.Common.Xaml.Wpf.Mvvm.Dialog;
using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;

namespace Orangebox.ShutdownTimer.BusinessLayer.Logic
{
    public interface ICloseHandler
    {
        bool CheckCanClose(bool isCountdownRunning, ScheduledPowerAction spa);
    }

    public class CloseHandler : ICloseHandler
    {
        private readonly IDialogService dialogService;
        private readonly IPowerActionExecutor powerActionExecutor;

        public CloseHandler(IDialogService dlgSvc, IPowerActionExecutor powExec)
        {
            dialogService = dlgSvc;
            powerActionExecutor = powExec;
        }

        public bool CheckCanClose(bool isCountdownRunning, ScheduledPowerAction spa)
        {
            if (!isCountdownRunning) return true;
            if (spa == null) return true;

            if (spa.PowerAction.IsSchedulable)
            {
                var response = dialogService.ShowYesNoCancel("You've scheduled a power action. Do you want to set it via the command line?\n\nTo abort it, you'll have to enter the following command:\n\nshutdown.exe /a", "Schedule?");
                if (response == null)
                    return false;
                else if (response == true)
                {
                    var waitTimeInSeconds = (Int32)(spa.TimeOfAction - DateTime.Now).TotalSeconds;
                    powerActionExecutor.Run(spa.PowerAction, waitTimeInSeconds);
                }
                return true;
            }
            else
            {
                var response = dialogService.ShowOkCancel("You've scheduled a power action that cannot be set via the command line. Closing this window will abort this power action. Do you want to proceed?\n\nOK: Abort the action and close the window.\nCancel: Keep the action and don't close the window.", "Action will be cancelled");
                return response;
            }
        }

    }
}
