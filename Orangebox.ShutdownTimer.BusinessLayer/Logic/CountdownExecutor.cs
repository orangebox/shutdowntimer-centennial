﻿using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;
using System.Threading.Tasks;

namespace Orangebox.ShutdownTimer.BusinessLayer.Logic
{
    public interface ICountdownExecutor
    {
        bool IsRunning { get; set; }

        Task<CountdownResult> RunTimer(DateTime timeOfAction, Action<TimeSpan> updateRemainingTime);
    }

    public class CountdownExecutor : ICountdownExecutor
    {
        public bool IsRunning { get; set; }

        private readonly TimeSpan ONE_SECOND = new TimeSpan(0, 0, 1);
        
        public Task<CountdownResult> RunTimer(DateTime timeOfAction, Action<TimeSpan> updateRemainingTime)
        {
            return Task.Run(async () =>
            {
                IsRunning = true;
                var remainingTime = timeOfAction - DateTime.Now;
                while (IsRunning && remainingTime.TotalSeconds > 0)
                {
                    await Task.Delay(ONE_SECOND);
                    remainingTime = timeOfAction - DateTime.Now;
                    updateRemainingTime(remainingTime);
                }
                return new CountdownResult(!IsRunning);
            });
        }
    }

}
