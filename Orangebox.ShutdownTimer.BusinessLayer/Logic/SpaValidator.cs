﻿using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;

namespace Orangebox.ShutdownTimer.BusinessLayer.Logic
{
    public interface ISpaValidator
    {
        SpaValidationResult Validate(ScheduledPowerAction spa);
    }

    public class SpaValidator : ISpaValidator
    {
        public static readonly TimeSpan LOWER_LIMIT = new TimeSpan(0, 1, 0);
        public static readonly TimeSpan UPPER_LIMIT = new TimeSpan(0, 0, 0, 315360000);

        public SpaValidationResult Validate(ScheduledPowerAction spa)
        {
            var currentTime = DateTime.Now;
            var message = String.Empty;
            var timeToAction = spa.TimeOfAction - currentTime;

            if (timeToAction < LOWER_LIMIT)
                message = $"The time must be greater than {currentTime.Add(LOWER_LIMIT)} (received {spa.TimeOfAction}).";
            else if (timeToAction > UPPER_LIMIT)
                message = $"The time must be less than {currentTime.Add(UPPER_LIMIT)} (received {spa.TimeOfAction}).";

            return new SpaValidationResult(message);
        }
    }
}
