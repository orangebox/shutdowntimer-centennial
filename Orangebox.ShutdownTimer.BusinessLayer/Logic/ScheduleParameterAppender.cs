﻿using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;

namespace Orangebox.ShutdownTimer.BusinessLayer.Logic
{
    public interface IScheduleParameterAppender
    {
        string AppendSchedule(PowerAction powerAction, int waitTimeInSeconds);
        string AppendSchedule(string parameters, int waitTimeInSeconds);
    }

    public class ScheduleParameterAppender : IScheduleParameterAppender
    {
        public string AppendSchedule(PowerAction powerAction, int waitTimeInSeconds)
        {
            if (!powerAction.IsSchedulable)
                return powerAction.Parameters;
            return AppendSchedule(powerAction.Parameters, waitTimeInSeconds);
        }

        public string AppendSchedule(string parameters, int waitTimeInSeconds)
        {
            if (String.IsNullOrEmpty(parameters))
                throw new ArgumentNullException($"Parameters are null or empty", nameof(parameters));
            return $"{parameters} /t {waitTimeInSeconds}";
        }

    }
}
