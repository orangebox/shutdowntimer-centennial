﻿using Orangebox.Common.Helpers;
using Orangebox.ShutdownTimer.BusinessLayer.Models;

namespace Orangebox.ShutdownTimer.BusinessLayer.Logic
{
    public interface IPowerActionExecutor
    {
        PowerActionResult Run(PowerAction powerAction);
        PowerActionResult Run(PowerAction powerAction, int waitTimeInSeconds);
    }

    public class PowerActionExecutor : IPowerActionExecutor
    {
        private readonly IProcessExecutor processExecutor;
        private readonly IScheduleParameterAppender scheduleParameterAppender;

        public PowerActionExecutor(IProcessExecutor procExec, IScheduleParameterAppender paramAppendr)
        {
            processExecutor = procExec;
            scheduleParameterAppender = paramAppendr;
        }

        public PowerActionResult Run(PowerAction powerAction)
        {
            return Run(powerAction, 0);
        }

        public PowerActionResult Run(PowerAction powerAction, int waitTimeInSeconds)
        {
            var parameters = scheduleParameterAppender.AppendSchedule(powerAction, waitTimeInSeconds);

            var exitCode = processExecutor.Run(powerAction.Filename, parameters);
            return new PowerActionResult(exitCode);
        }
    }

    public class DebugPowerActionExecutor : IPowerActionExecutor
    {
        private readonly IProcessExecutor processExecutor;

        public DebugPowerActionExecutor(IProcessExecutor procExec)
        {
            processExecutor = procExec;
        }

        public PowerActionResult Run(PowerAction powerAction)
        {
            processExecutor.Run("shutdown.exe", "/s /t 3600");
            var exitCode = processExecutor.Run("shutdown.exe", "/a");
            return new PowerActionResult(exitCode);
        }

        public PowerActionResult Run(PowerAction powerAction, int waitTimeInSeconds)
        {
            return Run(powerAction);
        }

    }
}
