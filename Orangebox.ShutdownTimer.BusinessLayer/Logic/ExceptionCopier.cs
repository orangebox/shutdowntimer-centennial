﻿using Orangebox.Common.Helpers;
using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System.Collections.Generic;
using System.Text;

namespace Orangebox.ShutdownTimer.BusinessLayer.Logic
{
    public interface IExceptionCopier
    {
        void CopyExceptions(List<ExceptionModel> exceptions);
        void CopyException(ExceptionModel exceptionModel);
    }

    public class ExceptionCopier : IExceptionCopier
    {
        #region Variables
        private readonly IClipboardHelper clipboardHelper;
        #endregion

        #region Constructors
        public ExceptionCopier(IClipboardHelper clipHelp)
        {
            clipboardHelper = clipHelp;
        }
        #endregion

        #region Public Methods
        public void CopyExceptions(List<ExceptionModel> exceptions)
        {
            const string SEPARATOR = "----------";
            var messageBuilder = new StringBuilder();
            messageBuilder.AppendLine($"Exceptions caught: {exceptions.Count}");
            messageBuilder.AppendLine(SEPARATOR);
            for (var index = 0; index < exceptions.Count; index++)
            {
                var exceptionModel = exceptions[index];
                messageBuilder.AppendLine($"#{index} Type: {exceptionModel.Type}");
                messageBuilder.AppendLine($"Message: {exceptionModel.Message}");
                messageBuilder.AppendLine($"Stack Trace:\n{exceptionModel.StackTrace}");
                messageBuilder.AppendLine(SEPARATOR);
            }
            clipboardHelper.Copy(messageBuilder.ToString());
        }

        public void CopyException(ExceptionModel exceptionModel)
        {
            var messageBuilder = new StringBuilder();
            messageBuilder.AppendLine($"Exception caught: {exceptionModel.Type}");
            messageBuilder.AppendLine($"Message: {exceptionModel.Message}");
            messageBuilder.AppendLine($"Stack Trace:\n{exceptionModel.StackTrace}");
            clipboardHelper.Copy(messageBuilder.ToString());
        }
        #endregion

    }
}
