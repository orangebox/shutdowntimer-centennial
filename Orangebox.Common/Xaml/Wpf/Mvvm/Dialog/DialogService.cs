﻿using System.Windows;

namespace Orangebox.Common.Xaml.Wpf.Mvvm.Dialog
{
    public class DialogService : IDialogService
    {
        public void ShowMessage(string message, string title)
        {
            MessageBox.Show(message, title, MessageBoxButton.OK);
        }

        public bool ShowYesNo(string message, string title)
        {
            var response = MessageBox.Show(message, title, MessageBoxButton.YesNo);
            return response == MessageBoxResult.Yes;
        }

        public bool? ShowYesNoCancel(string message, string title)
        {
            var response = MessageBox.Show(message, title, MessageBoxButton.YesNoCancel);
            if (response == MessageBoxResult.Yes)
                return true;
            else if (response == MessageBoxResult.No)
                return false;
            else
                return null;
        }

        public bool ShowOkCancel(string message, string title)
        {
            var response = MessageBox.Show(message, title, MessageBoxButton.OKCancel);
            return response == MessageBoxResult.OK;
        }

    }
}
