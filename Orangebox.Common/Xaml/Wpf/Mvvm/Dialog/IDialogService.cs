﻿
namespace Orangebox.Common.Xaml.Wpf.Mvvm.Dialog
{
    public interface IDialogService
    {
        void ShowMessage(string message, string title);
        bool ShowYesNo(string message, string title);
        bool? ShowYesNoCancel(string message, string title);
        bool ShowOkCancel(string message, string title);
    }
}
