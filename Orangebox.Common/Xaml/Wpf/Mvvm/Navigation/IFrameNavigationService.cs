﻿using System;

namespace Orangebox.Common.Xaml.Wpf.Mvvm.Navigation
{
    public interface IFrameNavigationService
    {
        string CurrentPageKey { get; set; }
        object PageParameter { get; }

        bool CanGoBack();
        bool CanGoForward();

        void GoBack();
        void GoForward();

        void NavigateTo(Type pageType);
        void NavigateTo(string pageKey);
        void NavigateTo(Type pageType, object parameter);
        void NavigateTo(string pageKey, object parameter);
    }
}
