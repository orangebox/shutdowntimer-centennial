﻿using System;

namespace Orangebox.Common.Xaml.Wpf.Mvvm.Navigation
{
    public interface IWindowNavigationService
    {
        object WindowParameter { get; }

        void OpenWindow(Type vmType, bool isTopMost, bool isDialog);
        void OpenWindow(Type vmType, object parameter, bool isTopMost, bool isDialog);

        void OpenWindow(string key, bool isTopMost, bool isDialog);
        void OpenWindow(string key, object parameter, bool isTopMost, bool isDialog);
    }
}
