﻿using System;

namespace Orangebox.Common.Xaml.Wpf.Mvvm.Navigation
{
    public class DesignFrameNavigationService : IFrameNavigationService
    {
        public string CurrentPageKey { get; set; }

        public object PageParameter { get; private set; }

        public bool CanGoBack()
        {
            return true;
        }

        public bool CanGoForward()
        {
            return true;
        }

        public void GoBack()
        {
            
        }

        public void GoForward()
        {

        }

        public void NavigateTo(string pageKey)
        {
            
        }

        public void NavigateTo(Type pageType)
        {
            
        }

        public void NavigateTo(string pageKey, object parameter)
        {
            
        }

        public void NavigateTo(Type pageType, object parameter)
        {
            
        }

    }
}
