﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;

namespace Orangebox.Common.Xaml.Wpf.Mvvm.Navigation
{
    public class FrameNavigationService : IFrameNavigationService, INotifyPropertyChanged
    {
        #region Variables
        private readonly Func<Frame> FRAME_GETTER;
        private readonly Dictionary<String, Uri> PAGES_BY_KEY;
        private Frame targetFrame;

        public object PageParameter { get; private set; }
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Binding Variables
        private string currentPageKey;
        public string CurrentPageKey
        {
            get { return currentPageKey; }
            set
            {
                if (currentPageKey == value) return;
                currentPageKey = value;
                RaisePropertyChanged(nameof(CurrentPageKey));
            }
        }
        #endregion

        #region Constructors
        public FrameNavigationService(Func<Frame> frameGetter)
        {
            FRAME_GETTER = frameGetter;
            PAGES_BY_KEY = new Dictionary<String, Uri>();
        }
        #endregion

        #region Private methods
        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private Frame GetTargetFrame()
        {
            if (targetFrame == null)
            {
                targetFrame = FRAME_GETTER();
            }
            return targetFrame;
        }

        private void UpdateCurrentPageKey()
        {
            lock (PAGES_BY_KEY)
            {
                var frame = GetTargetFrame();
                if (frame.Source == null)
                    CurrentPageKey = null;
                else
                {
                    var key = PAGES_BY_KEY.First(p => p.Value == frame.Source).Key;
                    CurrentPageKey = key;
                }
            }
        }
        #endregion

        #region Public methods
        public bool CanGoBack() => GetTargetFrame().CanGoBack;
        public bool CanGoForward() => GetTargetFrame().CanGoForward;

        public void GoBack()
        {
            if (!CanGoBack()) return;
            var frame = GetTargetFrame();
            frame.GoBack();
            UpdateCurrentPageKey();
        }

        public void GoForward()
        {
            if (!CanGoForward()) return;
            var frame = GetTargetFrame();
            frame.GoForward();
            UpdateCurrentPageKey();
        }

        public void NavigateTo(Type pageType) => NavigateTo(pageType.Name, null);

        public void NavigateTo(string pageKey) => NavigateTo(pageKey, null);

        public void NavigateTo(Type pageType, object parameter) => NavigateTo(pageType.Name, parameter);

        public void NavigateTo(string pageKey, object parameter)
        {
            lock (PAGES_BY_KEY)
            {
                if (!PAGES_BY_KEY.ContainsKey(pageKey))
                    throw new ArgumentException($"No such page: {pageKey}. Did you forget to call the Configure method?", nameof(pageKey));
                GetTargetFrame().Navigate(PAGES_BY_KEY[pageKey]);
                UpdateCurrentPageKey();
                PageParameter = parameter;
            }
        }

        public void ConfigurePage(Type vmKey, string pageUri) => ConfigurePage(vmKey.Name, new Uri(pageUri, UriKind.Relative));

        public void ConfigurePage(Type vmKey, Uri pageUri) => ConfigurePage(vmKey.Name, pageUri);

        public void ConfigurePage(string key, string pageUri) => ConfigurePage(key, new Uri(pageUri, UriKind.Relative));

        public void ConfigurePage(string key, Uri pageUri)
        {
            lock (PAGES_BY_KEY)
            {
                if (PAGES_BY_KEY.ContainsKey(key))
                    throw new ArgumentException($"This key has already been used: {key}", nameof(key));
                if (PAGES_BY_KEY.Any(p => p.Value == pageUri))
                    throw new ArgumentException($"This type has already been configured iwth key {PAGES_BY_KEY.First(p => p.Value == pageUri).Key}", nameof(pageUri));
                PAGES_BY_KEY.Add(key, pageUri);
            }
        }
        #endregion
    }
}
