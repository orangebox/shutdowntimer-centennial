﻿using System;

namespace Orangebox.Common.Xaml.Wpf.Mvvm.Navigation
{
    public class DesignWindowNavigationService : IWindowNavigationService
    {
        public object WindowParameter { get; private set; }

        public void OpenWindow(string key, bool isTopMost, bool isDialog) { }

        public void OpenWindow(Type vmType, bool isTopMost, bool isDialog) { }

        public void OpenWindow(string key, object parameter, bool isTopMost, bool isDialog) { }

        public void OpenWindow(Type vmType, object parameter, bool isTopMost, bool isDialog) { }

    }
}
