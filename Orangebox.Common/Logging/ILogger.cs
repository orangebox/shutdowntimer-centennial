﻿using System;

namespace Orangebox.Common.Logging
{
    public interface ILogger
    {
        void Initialize(Type type);

        void Trace(string message);
        void Debug(string message);
        void Info(string message);
        void Warn(string message);
        void Error(string message);
        void Fatal(string message);
        void Exception(Exception exception);
    }
}
