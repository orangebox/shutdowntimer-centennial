﻿using NLog;
using NLog.Config;
using NLog.Targets;
using Orangebox.Common.Extensions;
using System;
using System.Text;

namespace Orangebox.Common.Logging
{
    public class NLogLogger : ILogger
    {
        #region Variables
        private Logger logger;
        #endregion

        #region Constructors
        #endregion

        #region Private Methods
        private Target ConfigureConsoleTarget()
        {
            var consoleTarget = new ColoredConsoleTarget()
            {
                Layout = @"${date:format=HH\:mm\:ss} ${logger} ${message}"
            };
            return consoleTarget;
        }

        private Target ConfigureFileTarget()
        {
            var fileTarget = new FileTarget()
            {
                FileName = @"${basedir}output.log",
                Layout = @"${date:format=HH\:mm\:ss} ${logger} ${message}"
            };
            return fileTarget;
        }
        #endregion

        #region Public Methods
        public void Initialize(Type type)
        {
            logger = LogManager.GetLogger(type.Name);

            var config = new LoggingConfiguration();

#if DEBUG
            var consoleTarget = ConfigureConsoleTarget();
            var consoleRule = new LoggingRule("*", LogLevel.Trace, consoleTarget);
            config.AddTarget("console", consoleTarget);
            config.LoggingRules.Add(consoleRule);
#endif

            var fileTarget = ConfigureFileTarget();
#if DEBUG
            var fileRule = new LoggingRule("*", LogLevel.Trace, fileTarget);
#else
            var fileRule = new LoggingRule("*", LogLevel.Error, fileTarget);
#endif
            config.AddTarget("file", fileTarget);
            config.LoggingRules.Add(fileRule);

            LogManager.Configuration = config;
        }
        
        public void Trace(string message)
        {
            logger.Trace(message);
        }

        public void Debug(string message)
        {
            logger.Debug(message);
        }

        public void Info(string message)
        {
            logger.Info(message);
        }

        public void Warn(string message)
        {
            logger.Warn(message);
        }

        public void Error(string message)
        {
            logger.Error(message);
        }

        public void Exception(Exception exception)
        {
            const string SEPARATOR = "----------";
            var exceptionList = exception.Flatten();
            var messageBuilder = new StringBuilder();
            messageBuilder.Append($"Exceptions caught: {exceptionList.Count}");
            for (var index = 0; index < exceptionList.Count; index++)
            {
                var ex = exceptionList[index];
                messageBuilder.Append($"#{index + 1} {SEPARATOR}");
                messageBuilder.Append($"Type: {ex.GetType()}");
                messageBuilder.Append($"Message: {ex.Message}");
                messageBuilder.Append($"Stack Trace:\n{ex.StackTrace}");
                messageBuilder.Append(SEPARATOR);
            }
            logger.Error(messageBuilder.ToString());
        }

        public void Fatal(string message)
        {
            logger.Fatal(message);
        }
        #endregion
    }
}
