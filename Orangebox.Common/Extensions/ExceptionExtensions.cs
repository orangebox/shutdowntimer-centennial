﻿using System;
using System.Collections.Generic;

namespace Orangebox.Common.Extensions
{
    public static class ExceptionExtensions
    {
        public static List<Exception> Flatten(this Exception exception)
        {
            if (exception == null)
                return new List<Exception>();

            var exceptions = new List<Exception>();

            var currentException = exception;
            do
            {
                exceptions.Add(currentException);
                currentException = currentException.InnerException;
            } while (currentException != null);

            return exceptions;
        }
    }
}
