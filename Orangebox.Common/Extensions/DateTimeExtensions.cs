﻿using System;

namespace Orangebox.Common.Extensions
{
    public static class DateTimeExtensions
    {
        public static DateTime Trim(this DateTime dt, long roundTicks)
        {
            if (roundTicks == TimeSpan.TicksPerMillisecond)
                return dt;

            return new DateTime(dt.Ticks - dt.Ticks % roundTicks);
        }
    }

}
