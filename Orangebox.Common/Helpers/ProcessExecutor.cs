﻿using Orangebox.Common.Logging;
using System;
using System.Diagnostics;
using System.IO;

namespace Orangebox.Common.Helpers
{
    public interface IProcessExecutor
    {
        int Run(string filename, string parameters);
    }

    public class ProcessExecutor : IProcessExecutor
    {
        private readonly ILogger logger;

        public ProcessExecutor(ILogger logr)
        {
            logger = logr;
            logger.Initialize(GetType());
        }

        private Process BuildProcess()
        {
            var process = new Process();
            //process.StartInfo.WorkingDirectory = Directory.GetCurrentDirectory();
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.UseShellExecute = false;
#if DEBUG
            process.StartInfo.CreateNoWindow = false;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
#else
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
#endif
            process.OutputDataReceived += (s, e) =>
            {
                var line = e.Data;
                if (String.IsNullOrEmpty(line)) return;
                logger.Info($"OUTPUT: {line}");
            };
            process.StartInfo.RedirectStandardError = true;
            process.ErrorDataReceived += (s, e) =>
            {
                var line = e.Data;
                if (String.IsNullOrEmpty(line)) return;
                logger.Error($"ERROR: {line}");
            };
            return process;
        }

        public int Run(string filename, string parameters)
        {
            using (var process = BuildProcess())
            {
                process.StartInfo.FileName = filename;
                if (!String.IsNullOrEmpty(parameters))
                    process.StartInfo.Arguments = parameters;
                logger.Info($"Executing command: {filename} {parameters}");

                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();
                process.WaitForExit();
                process.CancelOutputRead();
                process.CancelErrorRead();

                var exitCode = process.ExitCode;
                logger.Info($"The command exited with exit code: {exitCode}");
                return exitCode;
            }
        }

    }
}
