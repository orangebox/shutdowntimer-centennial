﻿using System.Windows;

namespace Orangebox.Common.Helpers
{
    public interface IClipboardHelper
    {
        void Copy(string text);
    }

    public class ClipboardHelper : IClipboardHelper
    {
        public void Copy(string text)
        {
            Clipboard.SetText(text);
        }
    }
}
