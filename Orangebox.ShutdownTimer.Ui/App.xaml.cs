﻿using Orangebox.ShutdownTimer.Backend.ViewModels;
using System.Windows;
using System.Windows.Threading;

namespace Orangebox.ShutdownTimer.Ui
{
    public partial class App : Application
    {
        #region Static Variables
        public static App Instance { get { return App.Current as App; } }
        #endregion

        #region Variables
        public ViewModelLocator Locator { get; private set; }
        #endregion

        #region Constructors
        public App()
        {
            this.DispatcherUnhandledException += OnUnhandledException;
        }
        #endregion

        #region Event Handlers
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            this.Locator = this.Resources["Locator"] as ViewModelLocator;

            this.Locator.InitializeServices(() => (this.MainWindow as Views.MainWindow).MainFrame,
                                            () => (this.MainWindow as Views.MainWindow),
                                            typeof(Views.ExceptionWindow));
            this.Locator.InitializeViewModels();
        }
        
        private void OnUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            var exception = e.Exception;
            this.Locator.WindowNavigationService.OpenWindow(typeof(Views.ExceptionWindow), exception, false, true);
            e.Handled = true;
        }
        #endregion

    }
}
