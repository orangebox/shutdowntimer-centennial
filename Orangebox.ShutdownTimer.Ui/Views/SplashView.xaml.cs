﻿using System.Windows;
using System.Windows.Controls;

namespace Orangebox.ShutdownTimer.Ui.Views
{
    /// <summary>
    /// Interaction logic for SplashView.xaml
    /// </summary>
    public partial class SplashView : Page
    {
        private readonly MainWindow MAIN_WINDOW;

        public SplashView()
        {
            InitializeComponent();

            this.MAIN_WINDOW = App.Instance.MainWindow as MainWindow;

            this.Loaded += OnLoad;
            this.Unloaded += OnUnload;
        }

        private void OnLoad(object sender, RoutedEventArgs e)
        {
            this.MAIN_WINDOW.WindowStyle = WindowStyle.None;
        }

        private void OnUnload(object sender, RoutedEventArgs e)
        {
            this.MAIN_WINDOW.WindowStyle = WindowStyle.SingleBorderWindow;
        }

    }
}
