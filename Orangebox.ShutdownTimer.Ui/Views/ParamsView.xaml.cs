﻿using System.Windows.Controls;

namespace Orangebox.ShutdownTimer.Ui.Views
{
    /// <summary>
    /// Interaction logic for ParamsView.xaml
    /// </summary>
    public partial class ParamsView : Page
    {
        public ParamsView()
        {
            InitializeComponent();

            this.Loaded += (s, e) =>
            {
                this.SelectedDateTimeTextBox.Focus();
            };
        }

    }
}
