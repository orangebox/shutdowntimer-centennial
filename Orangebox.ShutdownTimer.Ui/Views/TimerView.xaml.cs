﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Orangebox.ShutdownTimer.Ui.Views
{
    public partial class TimerView : Page
    {
        #region Variables
        private const int INITIAL_AD_SIZE_INDEX = 6;
        private readonly List<Size> AD_SIZES = new List<Size>
        {
            //win10 & win8.1 sizes
            new Size(250.0, 250.0),
            new Size(300.0, 250.0),
            new Size(728.0, 90.0),
            new Size(160.0, 600.0),
            new Size(300.0, 600.0),
            //win10 mobile & wp8.x
            new Size(300.0, 50.0),
            new Size(320.0, 50.0),    //this is the size we're using in the UWP version
            new Size(480.0, 80.0),
            new Size(640.0, 100.0),
        };
        private int currentAdSizeIndex = INITIAL_AD_SIZE_INDEX;
        #endregion

        #region Constructors
        public TimerView()
        {
            InitializeComponent();

            Loaded += OnLoad;
#if DEBUG
            TimerAd.MouseDown += AdClicked;
#endif
        }
        #endregion

        #region Event handlers
        private void OnLoad(object sender, RoutedEventArgs e)
        {
            UpdateAdSize();
            UpdatePlaceholderText();
        }

        private void AdClicked(object sender, MouseButtonEventArgs e)
        {
            if (AD_SIZES.Count > 1)
            {
                currentAdSizeIndex++;
                if (currentAdSizeIndex > AD_SIZES.Count - 1)
                    currentAdSizeIndex = 0;

                UpdateAdSize();

                UpdatePlaceholderText();
            }
        }
        #endregion

        #region Private methods
        private void UpdateAdSize()
        {
            var newSize = AD_SIZES[currentAdSizeIndex];
            TimerAd.Width = newSize.Width;
            TimerAd.Height = newSize.Height;

            if (TimerAd.Width > TimerContentRoot.ActualWidth || TimerAd.Height > CountdownRow.ActualHeight)
                TimerAd.Style = Resources["AdBadBorderStyle"] as Style;
            else
                TimerAd.Style = Resources["AdOkBorderStyle"] as Style;
        }

        private void UpdatePlaceholderText()
        {
            var adSize = new Size(TimerAd.Width, TimerAd.Height);
            TimerAdText.Text = $"AD PLACEHOLDER ({adSize.Width} x {adSize.Height})";
        }
        #endregion

    }
}
