﻿using Orangebox.ShutdownTimer.Backend.ViewModels;
using System.ComponentModel;
using System.Windows;

namespace Orangebox.ShutdownTimer.Ui.Views
{
    public partial class MainWindow : Window
    {
        private readonly MainViewModel viewModel;

        public MainWindow()
        {
            InitializeComponent();
            viewModel = DataContext as MainViewModel;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            var canClose = viewModel.CheckCanClose();
            e.Cancel = !canClose;
        }

    }
}
