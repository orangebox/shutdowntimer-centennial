﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orangebox.ShutdownTimer.Backend.Converters;
using System;

namespace Orangebox.ShutdownTimer.Backend.Tests.Converters
{
    [TestClass]
    public class StringCaseConverterTests
    {
        #region Variables
        private StringCaseConverter stringCaseConverter;
        #endregion

        #region Setup
        [TestInitialize]
        public void BeforeEach()
        {
            stringCaseConverter = new StringCaseConverter();
        }
        #endregion

        #region When Converting
        [TestMethod]
        public void ConvertShouldConvertToLowerCase()
        {
            var inputString = "THIS IS AN INPUT STRING";

            var convertedString = stringCaseConverter.Convert(inputString, null, "l", null);

            convertedString.ShouldBeEquivalentTo(inputString.ToLower());
        }

        [TestMethod]
        public void ConvertShouldConvertToUpperCase()
        {
            var inputString = "this is an input string";

            var convertedString = stringCaseConverter.Convert(inputString, null, "u", null);

            convertedString.ShouldBeEquivalentTo(inputString.ToUpper());
        }

        [TestMethod]
        public void ConvertShouldHandleEmptyInputStrings()
        {
            var inputString = String.Empty;

            var convertedString = stringCaseConverter.Convert(inputString, null, "u", null);

            convertedString.ShouldBeEquivalentTo(String.Empty);
        }

        [TestMethod]
        public void ConvertShouldHandleNullStrings()
        {
            string inputString = null;

            var convertedString = stringCaseConverter.Convert(inputString, null, "u", null);

            convertedString.ShouldBeEquivalentTo(String.Empty);
        }

        [TestMethod]
        public void ConvertShouldHandleNullParameters()
        {
            var inputString = "this is AN INPUT STRING";

            var action = new Action(() => stringCaseConverter.Convert(inputString, null, null, null));

            action.ShouldThrow<NotImplementedException>();
        }

        [TestMethod]
        public void ConvertShouldHandleEmptyParameters()
        {
            var inputString = "THIS IS an input string";

            var action = new Action(() => stringCaseConverter.Convert(inputString, null, String.Empty, null));

            action.ShouldThrow<NotImplementedException>();
        }
        #endregion

        #region When Converting Back
        [TestMethod]
        public void ConvertBackShouldThrowException()
        {
            var action = new Action(() => stringCaseConverter.ConvertBack("", typeof(Int32), null, null));

            action.ShouldThrow<NotImplementedException>();
        }
        #endregion
    }
}
