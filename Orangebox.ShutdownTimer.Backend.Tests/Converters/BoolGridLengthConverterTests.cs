﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orangebox.ShutdownTimer.Backend.Converters;
using System.Windows;

namespace Orangebox.ShutdownTimer.Backend.Tests.Converters
{
    [TestClass]
    public class BoolGridLengthConverterTests
    {
        #region Variables
        private readonly GridLength VISIBLE_LENGTH = new GridLength(1, GridUnitType.Star);
        private readonly GridLength HIDDEN_LENGTH = new GridLength(0, GridUnitType.Pixel);

        private BoolGridLengthConverter boolGridLengthConverter;
        #endregion

        #region Setup
        [ClassInitialize]
        public static void BeforeAll(TestContext tc)
        {

        }

        [TestInitialize]
        public void BeforeEach()
        {
            boolGridLengthConverter = new BoolGridLengthConverter();
        }
        #endregion

        #region When Converting To
        [TestMethod]
        public void ConvertShouldReturnVisibleWhenTrue()
        {
            var gridLength = boolGridLengthConverter.Convert(true, null, null, null);

            gridLength.ShouldBeEquivalentTo(VISIBLE_LENGTH);
        }

        [TestMethod]
        public void ConvertShouldReturnHiddenWhenFalse()
        {
            var gridLength = boolGridLengthConverter.Convert(false, null, null, null);

            gridLength.ShouldBeEquivalentTo(HIDDEN_LENGTH);
        }

        [TestMethod]
        public void ConvertShouldHandleNullInput()
        {
            var gridLength = boolGridLengthConverter.Convert(null, null, null, null);

            gridLength.ShouldBeEquivalentTo(VISIBLE_LENGTH);
        }
        #endregion

        #region When Converting Back
        [TestMethod]
        public void ConvertBackShouldReturnTrueWhenVisible()
        {
            var isVisible = (bool)boolGridLengthConverter.ConvertBack(VISIBLE_LENGTH, null, null, null);

            isVisible.Should().BeTrue();
        }

        [TestMethod]
        public void ConvertBackShouldReturnFalseWhenHidden()
        {
            var isVisible = (bool)boolGridLengthConverter.ConvertBack(HIDDEN_LENGTH, null, null, null);

            isVisible.Should().BeFalse();
        }

        [TestMethod]
        public void ConvertBackShouldHandleNullInput()
        {
            var isVisible = (bool)boolGridLengthConverter.ConvertBack(null, null, null, null);

            isVisible.Should().BeTrue();
        }
        #endregion
    }
}
