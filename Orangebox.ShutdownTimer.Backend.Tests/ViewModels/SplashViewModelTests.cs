﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Orangebox.Common.Logging;
using Orangebox.Common.Xaml.Wpf.Mvvm.Navigation;
using Orangebox.ShutdownTimer.Backend.Helpers;
using Orangebox.ShutdownTimer.Backend.Models;
using Orangebox.ShutdownTimer.Backend.ViewModels;
using Orangebox.ShutdownTimer.BusinessLayer.Logic;
using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System.Threading.Tasks;

namespace Orangebox.ShutdownTimer.Backend.Tests.ViewModels
{
    [TestClass]
    public class SplashViewModelTests
    {
        #region Variables
        private readonly PowerAction ABORT_ACTION = new PowerAction("abort", "abort", "abort", "abort", false);
        private readonly VersionModel VERSION_MODEL = new VersionModel(123, 234, 345, 456);

        private IFrameNavigationService navService;
        private IPowerActionsRetriever powerActionsRetriever;
        private IPowerActionExecutor powerActionExecutor;
        private ILogger logger;
        private IVersionExtractor versionExtractor;
        private SplashViewModel splashViewModel;
        #endregion

        #region Setup
        [TestInitialize]
        public void BeforeEach()
        {
            navService = Substitute.For<IFrameNavigationService>();
            powerActionsRetriever = Substitute.For<IPowerActionsRetriever>();
            powerActionExecutor = Substitute.For<IPowerActionExecutor>();
            logger = Substitute.For<ILogger>();
            versionExtractor = Substitute.For<IVersionExtractor>();
            splashViewModel = new SplashViewModel(navService, 
                                                  powerActionsRetriever, 
                                                  powerActionExecutor, 
                                                  logger,
                                                  versionExtractor);
        }
        #endregion

        #region When Initializing
        [TestMethod]
        public void CtorShouldInitializeAllVariables()
        {
            splashViewModel.LoadedCommand.Should().NotBeNull();
        }
        #endregion

        #region When Loading
        [TestMethod]
        public void WhenLoadingShouldInitializeLogger()
        {
            versionExtractor.Extract().Returns(VERSION_MODEL);

            splashViewModel.LoadedCommand.Execute(null);

            logger.Received().Initialize(splashViewModel.GetType());
        }

        [TestMethod]
        public void WhenLoadingShouldSetVersion()
        {
            versionExtractor.Extract().Returns(VERSION_MODEL);

            splashViewModel.LoadedCommand.Execute(null);

            splashViewModel.Version.ShouldBeEquivalentTo($"v.{VERSION_MODEL.ToString()}");
        }

        [TestMethod]
        public void WhenLoadingShouldLogAbortAction()
        {
            versionExtractor.Extract().Returns(VERSION_MODEL);

            splashViewModel.LoadedCommand.Execute(null);

            logger.Received().Trace("Aborting any scheduled power actions");
        }

        [TestMethod]
        public async Task WhenLoadingShouldSubmitAbortAction()
        {
            versionExtractor.Extract().Returns(VERSION_MODEL);
            powerActionsRetriever.AbortAction.Returns(ABORT_ACTION);

            await Task.Run(() => splashViewModel.LoadedCommand.Execute(null));

            powerActionExecutor.Received().Run(ABORT_ACTION);
        }

        [TestMethod]
        public async Task WhenLoadingShouldNavToParams()
        {
            versionExtractor.Extract().Returns(VERSION_MODEL);
            powerActionsRetriever.AbortAction.Returns(ABORT_ACTION);

            await Task.Run(() => splashViewModel.LoadedCommand.Execute(null));
            await Task.Delay(2500);

            navService.Received().NavigateTo(typeof(ParamsViewModel));
        }
        #endregion
    }
}
