﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Orangebox.Common.Logging;
using Orangebox.Common.Xaml.Wpf.Mvvm.Dialog;
using Orangebox.Common.Xaml.Wpf.Mvvm.Navigation;
using Orangebox.ShutdownTimer.Backend.ViewModels;
using Orangebox.ShutdownTimer.BusinessLayer.Logic;
using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;
using System.Threading.Tasks;

namespace Orangebox.ShutdownTimer.Backend.Tests.ViewModels
{
    [TestClass]
    public class TimerViewModelTests
    {
        #region Variables
        private IFrameNavigationService navService;
        private IDialogService dialogService;
        private ICountdownExecutor countdownExecutor;
        private IPowerActionExecutor powerActionExecutor;
        private ILogger logger;
        private TimerViewModel timerViewModel;
        #endregion

        #region Setup
        [ClassInitialize]
        public static void BeforeAll(TestContext tc)
        {

        }

        [TestInitialize]
        public void BeforeEach()
        {
            navService = Substitute.For<IFrameNavigationService>();
            dialogService = Substitute.For<IDialogService>();
            countdownExecutor = Substitute.For<ICountdownExecutor>();
            powerActionExecutor = Substitute.For<IPowerActionExecutor>();
            logger = Substitute.For<ILogger>();
            timerViewModel = new TimerViewModel(navService, 
                                                dialogService, 
                                                countdownExecutor, 
                                                powerActionExecutor,
                                                logger);
        }
        #endregion

        #region Private Methods
        private PowerAction GetPa()
        {
            return new PowerAction("Sample PA Name",
                                   "Sample PA Filename",
                                   "Sample PA Parameters",
                                   "Sample PA Description",
                                   false);
        }

        private ScheduledPowerAction GetSpa()
        {
            var pa = GetPa();
            var dt = DateTime.Now.Add(new TimeSpan(1, 2, 3, 4, 5));
            return new ScheduledPowerAction(pa, dt);
        }

        private ScheduledPowerAction GetSpa(int days, int hours, int minutes)
        {
            var pa = GetPa();
            var dt = DateTime.Now.Add(new TimeSpan(days, hours, minutes, 1));
            return new ScheduledPowerAction(pa, dt);
        }
        #endregion

        #region When Initializing
        [TestMethod]
        public void CtorShouldInitializeVariables()
        {
            timerViewModel.LoadedCommand.Should().NotBeNull();
            timerViewModel.AbortCommand.Should().NotBeNull();
        }
        #endregion

        #region When Loading
        [TestMethod]
        public void WhenLoadingShouldInitializeLogger()
        {
            var spa = GetSpa();
            navService.PageParameter.Returns(spa);

            timerViewModel.LoadedCommand.Execute(null);

            logger.Received().Initialize(timerViewModel.GetType());
        }

        [TestMethod]
        public void WhenLoadingShouldGrabSpaFromNavSvc()
        {
            var spa = GetSpa();
            navService.PageParameter.Returns(spa);

            timerViewModel.LoadedCommand.Execute(null);

            timerViewModel.ScheduledPowerAction.ShouldBeEquivalentTo(spa);
        }

        [TestMethod]
        public void WhenLoadingShouldSetRemainingTime()
        {
            var spa = GetSpa();
            navService.PageParameter.Returns(spa);

            timerViewModel.LoadedCommand.Execute(null);
            
            timerViewModel.RemainingTime.Should().NotBe(default(TimeSpan));
        }

        [TestMethod]
        public void WhenLoadingShouldCallRunTimer()
        {
            var spa = GetSpa();
            navService.PageParameter.Returns(spa);

            timerViewModel.LoadedCommand.Execute(null);

            countdownExecutor.Received().RunTimer(Arg.Any<DateTime>(), Arg.Any<Action<TimeSpan>>());
        }
        #endregion

        #region When Setting Countdown Visibilities
        private void PrintValues()
        {
            Console.WriteLine($"Days - Visible: {timerViewModel.IsDaysVisible}, Value: {timerViewModel.RemainingTime.Days}");
            Console.WriteLine($"Hours - Visible: {timerViewModel.IsHoursVisible}, Value: {timerViewModel.RemainingTime.Hours}");
            Console.WriteLine($"Minutes - Visible: {timerViewModel.IsMinutesVisible}, Value: {timerViewModel.RemainingTime.Minutes}");
            Console.WriteLine($"Seconds - Visible: {true}, Value: {timerViewModel.RemainingTime.Seconds}");
        }

        [TestMethod]
        public void IsDaysVisibleShouldBeTrueWhenDaysIsGreaterThanZero()
        {
            var spa = GetSpa(1, 0, 0);
            navService.PageParameter.Returns(spa);

            timerViewModel.LoadedCommand.Execute(null);
            PrintValues();

            timerViewModel.IsDaysVisible.Should().BeTrue($"days = {timerViewModel.RemainingTime.Days}");
            timerViewModel.IsHoursVisible.Should().BeTrue($"hours = {timerViewModel.RemainingTime.Hours}");
            timerViewModel.IsMinutesVisible.Should().BeTrue($"minutes = {timerViewModel.RemainingTime.Minutes}");
        }

        [TestMethod]
        public void IsDaysVisibleShouldBeFalseWhenDaysIsZero()
        {
            var spa = GetSpa(0, 1, 0);
            navService.PageParameter.Returns(spa);

            timerViewModel.LoadedCommand.Execute(null);
            PrintValues();

            timerViewModel.IsDaysVisible.Should().BeFalse($"days = {timerViewModel.RemainingTime.Days}");
            timerViewModel.IsHoursVisible.Should().BeTrue($"hours = {timerViewModel.RemainingTime.Hours}");
            timerViewModel.IsMinutesVisible.Should().BeTrue($"minutes = {timerViewModel.RemainingTime.Minutes}");
        }

        [TestMethod]
        public void IsHoursVisibleShouldBeTrueWhenHoursIsGreaterThanZero()
        {
            var spa = GetSpa(0, 1, 0);
            navService.PageParameter.Returns(spa);

            timerViewModel.LoadedCommand.Execute(null);
            PrintValues();

            timerViewModel.IsDaysVisible.Should().BeFalse($"days = {timerViewModel.RemainingTime.Days}");
            timerViewModel.IsHoursVisible.Should().BeTrue($"hours = {timerViewModel.RemainingTime.Hours}");
            timerViewModel.IsMinutesVisible.Should().BeTrue($"minutes = {timerViewModel.RemainingTime.Minutes}");
        }

        [TestMethod]
        public void IsHoursVisibleShouldbeFalseWhenHoursIsZero()
        {
            var spa = GetSpa(0, 0, 0);
            navService.PageParameter.Returns(spa);

            timerViewModel.LoadedCommand.Execute(null);
            PrintValues();

            timerViewModel.IsDaysVisible.Should().BeFalse($"days = {timerViewModel.RemainingTime.Days}");
            timerViewModel.IsHoursVisible.Should().BeFalse($"hours = {timerViewModel.RemainingTime.Hours}");
            timerViewModel.IsMinutesVisible.Should().BeFalse($"minutes = {timerViewModel.RemainingTime.Minutes}");
        }

        [TestMethod]
        public void IsMinutesVisibleShouldBeTrueWhenMinutesIsGreaterThanZero()
        {
            var spa = GetSpa(0, 0, 1);
            navService.PageParameter.Returns(spa);

            timerViewModel.LoadedCommand.Execute(null);
            PrintValues();

            timerViewModel.IsDaysVisible.Should().BeFalse($"days = {timerViewModel.RemainingTime.Days}");
            timerViewModel.IsHoursVisible.Should().BeFalse($"hours = {timerViewModel.RemainingTime.Hours}");
            timerViewModel.IsMinutesVisible.Should().BeTrue($"minutes = {timerViewModel.RemainingTime.Minutes}");
        }

        [TestMethod]
        public void IsMinutesVisibleShouldBeFalseWhenMinutesIsZero()
        {
            var spa = GetSpa(0, 0, 0);
            navService.PageParameter.Returns(spa);

            timerViewModel.LoadedCommand.Execute(null);
            PrintValues();

            timerViewModel.IsDaysVisible.Should().BeFalse($"days = {timerViewModel.RemainingTime.Days}");
            timerViewModel.IsHoursVisible.Should().BeFalse($"hours = {timerViewModel.RemainingTime.Hours}");
            timerViewModel.IsMinutesVisible.Should().BeFalse($"minutes = {timerViewModel.RemainingTime.Minutes}");
        }
        #endregion

        #region When Timer Finishes
        [TestMethod]
        public async Task WhenTimerFinishesShouldCallProcessExecutor()
        {
            var spa = GetSpa(0, 0, 0);
            navService.PageParameter.Returns(spa);
            countdownExecutor.RunTimer(Arg.Any<DateTime>(), Arg.Any<Action<TimeSpan>>()).Returns(new CountdownResult(false));

            await Task.Run(() => timerViewModel.LoadedCommand.Execute(null));

            powerActionExecutor.Received().Run(spa.PowerAction);
        }
        #endregion

        #region When Aborting
        [TestMethod]
        public void AbortCommandShouldCallLogger()
        {
            timerViewModel.ScheduledPowerAction = GetSpa();
            countdownExecutor.IsRunning.Returns(true);

            timerViewModel.AbortCommand.Execute(null);

            logger.Received().Info(Arg.Any<String>());
        }

        [TestMethod]
        public void AbortCommandShouldSetIsRunningFlag()
        {
            countdownExecutor.IsRunning.Returns(true);
            timerViewModel.ScheduledPowerAction = GetSpa();

            timerViewModel.AbortCommand.Execute(null);

            countdownExecutor.IsRunning.Should().BeFalse();
        }

        [TestMethod]
        public void AbortCommandShouldGoBack()
        {
            timerViewModel.ScheduledPowerAction = GetSpa();
            countdownExecutor.IsRunning.Returns(true);

            timerViewModel.AbortCommand.Execute(null);

            navService.Received().GoBack();
#if DEBUG
            dialogService.DidNotReceive().ShowMessage(Arg.Any<String>(), Arg.Any<String>());
            navService.DidNotReceive().NavigateTo(typeof(ParamsViewModel));
#endif
        }

        [TestMethod]
        public void AbortCanExecuteShouldReturnTrueWhenCountingDown()
        {
            countdownExecutor.IsRunning.Returns(true);

            var canExecute = timerViewModel.AbortCommand.CanExecute(null);

            canExecute.Should().BeTrue();
        }

        [TestMethod]
        public void AbortCanExecuteShouldReturnFalseWhenNotCountingDown()
        {
            countdownExecutor.IsRunning.Returns(false);

            var canExecute = timerViewModel.AbortCommand.CanExecute(null);

            canExecute.Should().BeFalse();
        }
        #endregion
    }
}
