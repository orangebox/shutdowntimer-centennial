﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Orangebox.Common.Extensions;
using Orangebox.Common.Logging;
using Orangebox.Common.Xaml.Wpf.Mvvm.Navigation;
using Orangebox.ShutdownTimer.Backend.ViewModels;
using Orangebox.ShutdownTimer.BusinessLayer.Logic;
using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Orangebox.ShutdownTimer.Backend.Tests.ViewModels
{
    [TestClass]
    public class ExceptionViewModelTests
    {
        #region Variables
        private IWindowNavigationService windowNavService;
        private ILogger logger;
        private IExceptionCopier exceptionCopier;
        private ExceptionViewModel exceptionViewModel;
        #endregion

        #region Setup
        [TestInitialize]
        public void BeforeEach()
        {
            windowNavService = Substitute.For<IWindowNavigationService>();
            logger = Substitute.For<ILogger>();
            exceptionCopier = Substitute.For<IExceptionCopier>();
            exceptionViewModel = new ExceptionViewModel(windowNavService, 
                                                        logger, 
                                                        exceptionCopier);
        }
        #endregion

        #region Private Methods
        private List<ExceptionModel> GetExceptionModels()
        {
            var exceptions = new List<ExceptionModel>();
            for(var index = 0; index < 5; index++)
            {
                var exception = new ExceptionModel(typeof(Exception), $"message {index + 1}", "stack trace");
                exceptions.Add(exception);
            }
            return exceptions;
        }
        #endregion

        #region When Initializing
        [TestMethod]
        public void CtorShouldInitializeCommands()
        {
            exceptionViewModel.LoadedCommand.Should().NotBeNull();
            exceptionViewModel.CopySingleCommand.Should().NotBeNull();
            exceptionViewModel.CopyAllCommand.Should().NotBeNull();
        }
        #endregion

        #region When Loading
        [TestMethod]
        public void LoadedCommandShouldGetExceptionFromNav()
        {
            var exception = new Exception("sample");
            windowNavService.WindowParameter.Returns(exception);

            exceptionViewModel.LoadedCommand.Execute(null);

            exceptionViewModel.Exceptions.ShouldBeEquivalentTo(exception.Flatten().Select(ex => new ExceptionModel(ex)).ToList());
        }

        [TestMethod]
        public void LoadedCommandShouldInitializeLogger()
        {
            exceptionViewModel.LoadedCommand.Execute(null);

            logger.Received().Initialize(exceptionViewModel.GetType());
        }

        [TestMethod]
        public void LoadedCommandShouldLogExceptions()
        {
            exceptionViewModel.LoadedCommand.Execute(null);

            logger.Received().Exception(Arg.Any<Exception>());
        }
        #endregion

        #region When Copying Single Exception
        [TestMethod]
        public void CopySingleShouldCallExceptionCopier()
        {
            var exceptions = GetExceptionModels();
            exceptionViewModel.Exceptions = exceptions;

            exceptionViewModel.CopySingleCommand.Execute(exceptions.First());

            exceptionCopier.Received().CopyException(exceptions.First());
        }
        #endregion

        #region When Copying All Exceptions
        [TestMethod]
        public void CopyAllShouldCallExceptionCopier()
        {
            var exceptions = GetExceptionModels();
            exceptionViewModel.Exceptions = exceptions;

            exceptionViewModel.CopyAllCommand.Execute(null);

            exceptionCopier.Received().CopyExceptions(exceptions);
        }
        #endregion
    }
}
