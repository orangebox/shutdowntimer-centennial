﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Orangebox.Common.Logging;
using Orangebox.Common.Xaml.Wpf.Mvvm.Navigation;
using Orangebox.ShutdownTimer.Backend.Helpers;
using Orangebox.ShutdownTimer.Backend.Models;
using Orangebox.ShutdownTimer.Backend.ViewModels;
using Orangebox.ShutdownTimer.BusinessLayer.Logic;
using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;

namespace Orangebox.ShutdownTimer.Backend.Tests.ViewModels
{
    [TestClass]
    public class MainViewModelTests
    {
        #region Variables
        private readonly VersionModel VERSION_MODEL = new VersionModel(123, 234, 345, 456);
        private IFrameNavigationService navService;
        private IVersionExtractor versionExtractor;
        private ILogger logger;
        private ICountdownExecutor countdownExecutor;
        private ICloseHandler closeHandler;
        private MainViewModel mainViewModel;
        #endregion

        #region Setup
        [TestInitialize]
        public void BeforeEach()
        {
            navService = Substitute.For<IFrameNavigationService>();
            versionExtractor = Substitute.For<IVersionExtractor>();
            logger = Substitute.For<ILogger>();
            countdownExecutor = Substitute.For<ICountdownExecutor>();
            closeHandler = Substitute.For<ICloseHandler>();
            mainViewModel = new MainViewModel(navService, 
                                              versionExtractor, 
                                              logger, 
                                              countdownExecutor, 
                                              closeHandler);
        }
        #endregion

        #region When Initializing
        [TestMethod]
        public void CtorShouldInitializeVariables()
        {
            mainViewModel.LoadedCommand.Should().NotBeNull();
        }
        #endregion

        #region When Loading
        [TestMethod]
        public void WhenLoadingShouldInitializeLogger()
        {
            versionExtractor.Extract().Returns(VERSION_MODEL);

            mainViewModel.LoadedCommand.Execute(null);

            logger.Received().Initialize(mainViewModel.GetType());
        }

        [TestMethod]
        public void WhenLoadingShouldCallVersionExtractor()
        {
            versionExtractor.Extract().Returns(VERSION_MODEL);

            mainViewModel.LoadedCommand.Execute(null);

            versionExtractor.Received().Extract();
        }

        [TestMethod]
        public void WhenLoadingShouldSetTitleToVersion()
        {
            versionExtractor.Extract().Returns(VERSION_MODEL);

            mainViewModel.LoadedCommand.Execute(null);

            mainViewModel.Title.Should().Contain(VERSION_MODEL.ToString());
        }

        [TestMethod]
        public void WhenLoadingShouldLogVersion()
        {
            versionExtractor.Extract().Returns(VERSION_MODEL);

            mainViewModel.LoadedCommand.Execute(null);

            logger.Received().Info(mainViewModel.Title);
        }

        [TestMethod]
        public void WhenLoadingShouldNavigateToSplash()
        {
            versionExtractor.Extract().Returns(VERSION_MODEL);

            mainViewModel.LoadedCommand.Execute(null);

            navService.Received().NavigateTo(typeof(SplashViewModel));
        }
        #endregion

        #region When Trying To Close
        private ScheduledPowerAction GetSpa()
        {
            var pa = new PowerAction("", "", "", "", false);
            return new ScheduledPowerAction(pa, DateTime.Now);
        }

        [TestMethod]
        public void OnCloseShouldCallCloseHandler()
        {
            var spa = GetSpa();
            countdownExecutor.IsRunning.Returns(true);
            navService.PageParameter.Returns(spa);

            mainViewModel.CheckCanClose();

            closeHandler.Received().CheckCanClose(countdownExecutor.IsRunning, spa);
        }

        [TestMethod]
        public void OnCloseShouldReturnCheckCanClose()
        {
            var spa = GetSpa();
            countdownExecutor.IsRunning.Returns(true);
            navService.PageParameter.Returns(spa);
            closeHandler.CheckCanClose(countdownExecutor.IsRunning, spa).Returns(!default(bool));

            var canClose = mainViewModel.CheckCanClose();

            canClose.ShouldBeEquivalentTo(closeHandler.CheckCanClose(countdownExecutor.IsRunning, spa));
        }
        #endregion
    }
}
