﻿using FluentAssertions;
using Microsoft.Practices.ServiceLocation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orangebox.ShutdownTimer.Backend.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;

namespace Orangebox.ShutdownTimer.Backend.Tests.ViewModels
{
    [TestClass]
    public class ViewModelLocatorTests
    {
        #region Variables
        private ViewModelLocator viewModelLocator;
        private readonly Func<Frame> FRAME_GETTER = new Func<Frame>(() => new Frame());
        private readonly Func<Window> WINDOW_GETTER = new Func<Window>(() => new Window());
        #endregion

        #region Setup
        [TestInitialize]
        public void BeforeEach()
        {
            viewModelLocator = new ViewModelLocator();
        }

        [TestCleanup]
        public void AfterEach()
        {
            GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.Reset();
        }
        #endregion

        #region Private Methods
        private Assembly GetAssembly(string assemblyName)
        {
            var currentDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var assembly = Assembly.LoadFile($"{currentDir}\\{assemblyName}.dll");
            return assembly;
        }

        private IEnumerable<Type> GetInterfacesInAssembly(Assembly assembly)
        {
            var interfaces = assembly.GetTypes().Where(t => t.IsInterface);
            foreach (var @interface in interfaces)
            {
                Console.WriteLine($"Interface: {@interface.FullName}");
            }
            return interfaces;
        }

        private IEnumerable<Type> GetClassesInAssembly(Assembly assembly)
        {
            var classes = assembly.GetTypes().Where(t => t.IsClass);
            foreach (var @class in classes)
            {
                Console.WriteLine($"Class: {@class.FullName}");
            }
            return classes;
        }

        private object GetInstance(Type type)
        {
            Console.WriteLine($"Obtaining type: {type.FullName}");
            return ServiceLocator.Current.GetInstance(type);
        }
        #endregion

        #region When Initializing
        [TestMethod]
        public void CtorShouldSetIocContainer()
        {
            ServiceLocator.Current.Should().NotBeNull();
        }

        [TestMethod]
        public void InitializeMethodShouldRegisterAllBusinessLogicServices()
        {
            var businessAssembly = GetAssembly("Orangebox.ShutdownTimer.BusinessLayer");
            var bizServices = GetInterfacesInAssembly(businessAssembly)
                                .Where(t => t.Namespace.Contains("Logic"));

            viewModelLocator.InitializeServices(FRAME_GETTER, WINDOW_GETTER, typeof(Int32));

            bizServices.Should().NotBeEmpty();
            foreach (var bizService in bizServices)
            {
                var because = $"{bizService.FullName} does not exist in the IOC container";
                GetInstance(bizService).Should().NotBeNull(because);
            }
        }

        [TestMethod]
        public void InitializeMethodShouldRegisterAllCommonServices()
        {
            var commonAssembly = GetAssembly("Orangebox.Common");
            var commonServices = GetInterfacesInAssembly(commonAssembly)
                                    .Where(t => t.Namespace.Contains("Helpers") ||
                                                t.Namespace.Contains("Logging") ||
                                                t.Namespace.Contains("Dialog") ||
                                                t.Namespace.Contains("Navigation"));

            viewModelLocator.InitializeServices(FRAME_GETTER, WINDOW_GETTER, typeof(Int32));

            commonServices.Should().NotBeEmpty();
            foreach (var commonService in commonServices)
            {
                var because = $"{commonService.FullName} does not exist in the IOC container";
                GetInstance(commonService).Should().NotBeNull();
            }
        }

        [TestMethod]
        public void InitializeMethodShouldRegisterAllViewModels()
        {
            var backendAssembly = GetAssembly("Orangebox.ShutdownTimer.Backend");
            var viewModels = GetClassesInAssembly(backendAssembly)
                            .Where(t => t.BaseType == typeof(GalaSoft.MvvmLight.ViewModelBase));

            viewModelLocator.InitializeServices(FRAME_GETTER, WINDOW_GETTER, typeof(Int32));
            viewModelLocator.InitializeViewModels();

            viewModels.Should().NotBeEmpty();
            foreach (var viewModel in viewModels)
            {
                var because = $"{viewModel.FullName} does not exist in the IOC container";
                GetInstance(viewModel).Should().NotBeNull();
            }
        }

        [TestMethod]
        public void InitializeMethodShouldConfigureAllNavigablePages()
        {
            var backendAssembly = GetAssembly("Orangebox.ShutdownTimer.Backend");
            var navigableViewModels = GetClassesInAssembly(backendAssembly)
                                        .Where(t => t.BaseType == typeof(GalaSoft.MvvmLight.ViewModelBase) &&
                                                    t != typeof(MainViewModel) &&
                                                    t != typeof(ExceptionViewModel));

            viewModelLocator.InitializeServices(FRAME_GETTER, WINDOW_GETTER, typeof(Int32));

            var navService = viewModelLocator.FrameNavigationService;
            foreach (var navigableViewModel in navigableViewModels)
            {
                navService.NavigateTo(navigableViewModel);
            }
        }
        #endregion
    }
}
