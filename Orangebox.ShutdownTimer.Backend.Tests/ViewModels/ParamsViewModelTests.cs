﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Orangebox.Common.Logging;
using Orangebox.Common.Xaml.Wpf.Mvvm.Dialog;
using Orangebox.Common.Xaml.Wpf.Mvvm.Navigation;
using Orangebox.ShutdownTimer.Backend.Helpers;
using Orangebox.ShutdownTimer.Backend.Models;
using Orangebox.ShutdownTimer.Backend.ViewModels;
using Orangebox.ShutdownTimer.BusinessLayer.Logic;
using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Orangebox.ShutdownTimer.Backend.Tests.ViewModels
{
    [TestClass]
    public class ParamsViewModelTests
    {
        #region Variables
        private IDialogService dialogService;
        private IFrameNavigationService navService;
        private IPowerActionsRetriever powerActionsRetriever;
        private ISpaValidator spaValidator;
        private IVersionExtractor versionExtractor;
        private ILogger logger;
        private ParamsViewModel paramsViewModel;
        #endregion

        #region Setup
        [TestInitialize]
        public void BeforeEach()
        {
            dialogService = Substitute.For<IDialogService>();
            navService = Substitute.For<IFrameNavigationService>();
            powerActionsRetriever = Substitute.For<IPowerActionsRetriever>();
            spaValidator = Substitute.For<ISpaValidator>();
            versionExtractor = Substitute.For<IVersionExtractor>();
            logger = Substitute.For<ILogger>();
            paramsViewModel = new ParamsViewModel(navService, 
                                                  powerActionsRetriever, 
                                                  spaValidator, 
                                                  dialogService, 
                                                  versionExtractor,
                                                  logger);
        }
        #endregion

        #region Private methods
        private PowerAction GetSamplePowerAction()
        {
            return new PowerAction("", "", "", "", false);
        }
        #endregion

        #region When Initializing
        [TestMethod]
        public void CtorShouldInitializeVariables()
        {
            paramsViewModel.LoadedCommand.Should().NotBeNull();
        }
        #endregion

        #region WhenLoading
        [TestMethod]
        public void WhenLoadingShouldInitializeLogger()
        {
            var powerActions = new List<PowerAction>
            {
                GetSamplePowerAction()
            };
            powerActionsRetriever.GetPowerActions().Returns(powerActions);

            paramsViewModel.LoadedCommand.Execute(null);

            logger.Received().Initialize(paramsViewModel.GetType());
        }

        [TestMethod]
        public void WhenLoadingShouldGetPowerActions()
        {
            var powerActions = new List<PowerAction>
            {
                GetSamplePowerAction()
            };
            powerActionsRetriever.GetPowerActions().Returns(powerActions);

            paramsViewModel.LoadedCommand.Execute(null);

            paramsViewModel.PowerActions.ShouldBeEquivalentTo(powerActions);
        }

        [TestMethod]
        public void WhenLoadingShouldSelectFirstAction()
        {
            var powerActions = new List<PowerAction>
            {
                GetSamplePowerAction()
            };
            powerActionsRetriever.GetPowerActions().Returns(powerActions);

            paramsViewModel.LoadedCommand.Execute(null);

            paramsViewModel.SelectedPowerAction.ShouldBeEquivalentTo(powerActions.First());
        }

        [TestMethod]
        public void WhenLoadingShouldSetSelectedDate()
        {
            var powerActions = new List<PowerAction>
            {
                GetSamplePowerAction()
            };
            powerActionsRetriever.GetPowerActions().Returns(powerActions);

            paramsViewModel.LoadedCommand.Execute(null);

            paramsViewModel.SelectedDateTime.Should().NotBe(default(DateTime));
        }
        #endregion

        #region When About
        [TestMethod]
        public void AboutCommandShouldCallVersionExtractor()
        {
            var version = new VersionModel(123, 234, 345, 456);
            versionExtractor.Extract().Returns(version);

            paramsViewModel.AboutCommand.Execute(null);

            versionExtractor.Received().Extract();
        }

        [TestMethod]
        public void AboutCommandShouldShowMessageWithVersion()
        {
            var version = new VersionModel(123, 234, 345, 456);
            versionExtractor.Extract().Returns(version);

            paramsViewModel.AboutCommand.Execute(null);

            dialogService.Received().ShowMessage($"Version: {version.FullVersion}", Arg.Any<String>());
        }
        #endregion

        #region When Submitting
        [TestMethod]
        public void SubmitShouldLogValidActions()
        {
            paramsViewModel.SelectedDateTime = DateTime.Now;
            paramsViewModel.SelectedPowerAction = GetSamplePowerAction();
            spaValidator.Validate(Arg.Any<ScheduledPowerAction>()).Returns(new SpaValidationResult(String.Empty));

            paramsViewModel.SubmitCommand.Execute(null);

            logger.Received().Info(Arg.Any<String>());
        }

        [TestMethod]
        public void SubmitShouldNotLogInvalidActions()
        {
            paramsViewModel.SelectedDateTime = DateTime.Now;
            paramsViewModel.SelectedPowerAction = GetSamplePowerAction();
            spaValidator.Validate(Arg.Any<ScheduledPowerAction>()).Returns(new SpaValidationResult("err"));

            paramsViewModel.SubmitCommand.Execute(null);

            logger.DidNotReceive().Info(Arg.Any<String>());
        }

        [TestMethod]
        public void SubmitShouldOnlyBeEnabledAfterSettingParams()
        {
            paramsViewModel.SelectedDateTime = DateTime.Now;
            paramsViewModel.SelectedPowerAction = GetSamplePowerAction();

            var canExecute = paramsViewModel.SubmitCommand.CanExecute(null);

            canExecute.Should().BeTrue();
        }

        [TestMethod]
        public void SubmitShouldRequireSelectedPowerAction()
        {
            paramsViewModel.SelectedDateTime = DateTime.Now;
            paramsViewModel.SelectedPowerAction = null;

            var canExecute = paramsViewModel.SubmitCommand.CanExecute(null);

            canExecute.Should().BeFalse();
        }

        [TestMethod]
        public void SubmitShouldRequireSelectedDateTime()
        {
            paramsViewModel.SelectedDateTime = default(DateTime);
            paramsViewModel.SelectedPowerAction = GetSamplePowerAction();

            var canExecute = paramsViewModel.SubmitCommand.CanExecute(null);

            canExecute.Should().BeFalse();
        }

        [TestMethod]
        public void WhenSubmittingShouldValidateParams()
        {
            paramsViewModel.SelectedPowerAction = GetSamplePowerAction();
            paramsViewModel.SelectedDateTime = DateTime.Now;
            var spa = new ScheduledPowerAction(paramsViewModel.SelectedPowerAction, paramsViewModel.SelectedDateTime);

            spaValidator.Validate(spa).Returns(new SpaValidationResult(String.Empty));

            paramsViewModel.SubmitCommand.Execute(null);

            spaValidator.Received().Validate(spa);
        }

        [TestMethod]
        public void WhenSubmittingGoodParamsShouldNavigateToTimer()
        {
            paramsViewModel.SelectedPowerAction = GetSamplePowerAction();
            paramsViewModel.SelectedDateTime = DateTime.Now;
            var spa = new ScheduledPowerAction(paramsViewModel.SelectedPowerAction, paramsViewModel.SelectedDateTime);

            var validationResult = new SpaValidationResult(String.Empty);
            spaValidator.Validate(Arg.Any<ScheduledPowerAction>()).Returns(validationResult);

            paramsViewModel.SubmitCommand.Execute(null);

            dialogService.DidNotReceive().ShowMessage(Arg.Any<String>(), Arg.Any<String>());
            navService.Received().NavigateTo(typeof(TimerViewModel), spa);
        }

        [TestMethod]
        public void WhenSubmittingBadParamsShouldDisplayDialog()
        {
            paramsViewModel.SelectedPowerAction = GetSamplePowerAction();
            paramsViewModel.SelectedDateTime = DateTime.Now;
            var validationResult = new SpaValidationResult("sample error");
            spaValidator.Validate(Arg.Any<ScheduledPowerAction>()).Returns(validationResult);

            paramsViewModel.SubmitCommand.Execute(null);

            dialogService.Received().ShowMessage(validationResult.ErrorMessage, Arg.Any<String>());
            navService.DidNotReceive().NavigateTo(Arg.Any<String>());
        }
        #endregion
    }
}
