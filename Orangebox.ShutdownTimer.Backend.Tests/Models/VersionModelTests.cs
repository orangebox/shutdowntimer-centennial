﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orangebox.ShutdownTimer.Backend.Models;

namespace Orangebox.ShutdownTimer.Backend.Tests.Models
{
    [TestClass]
    public class VersionModelTests
    {
        #region When Initializing
        [TestMethod]
        public void CtorShouldSetMajor()
        {
            var major = 123;
            var versionModel = new VersionModel(major, 0, 0, 0);
            versionModel.Major.ShouldBeEquivalentTo(major);
        }

        [TestMethod]
        public void CtorShouldSetMinor()
        {
            var minor = 123;
            var versionModel = new VersionModel(0, minor, 0, 0);
            versionModel.Minor.ShouldBeEquivalentTo(minor);
        }

        [TestMethod]
        public void CtorShouldSetBuild()
        {
            var build = 123;
            var versionModel = new VersionModel(0, 0, build, 0);
            versionModel.Build.ShouldBeEquivalentTo(build);
        }

        [TestMethod]
        public void CtorShouldSetRevision()
        {
            var revision = 123;
            var versionModel = new VersionModel(0, 0, 0, revision);
            versionModel.Revision.ShouldBeEquivalentTo(revision);
        }
        #endregion

        #region When Getting String
        [TestMethod]
        public void FullVersionShouldReturnAllComponents()
        {
            var major = 123;
            var minor = 234;
            var build = 345;
            var revision = 456;
            var expectedString = $"{major}.{minor}.{build}.{revision}";

            var versionModel = new VersionModel(major, minor, build, revision);

            versionModel.FullVersion.ShouldBeEquivalentTo(expectedString);
        }

        [TestMethod]
        public void ToStringShouldReturnAppropriateParts()
        {
            var major = 123;
            var minor = 234;
            var build = 345;
            var revision = 456;
#if DEBUG
            var expectedString = $"{major}.{minor}.{build}.{revision}";
#else
            
            var expectedString = $"{major}.{minor}";
#endif

            var versionModel = new VersionModel(major, minor, build, revision);

            versionModel.ToString().ShouldBeEquivalentTo(expectedString);
        }
        #endregion
    }
}
