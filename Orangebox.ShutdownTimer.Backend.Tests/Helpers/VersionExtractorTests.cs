﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orangebox.ShutdownTimer.Backend.Helpers;
using System;
using System.Reflection;

namespace Orangebox.ShutdownTimer.Backend.Tests.Helpers
{
    [TestClass]
    public class VersionExtractorTests
    {
        #region Variables
        private VersionExtractor versionExtractor;
        #endregion

        #region Setup
        [TestInitialize]
        public void BeforeEach()
        {
            versionExtractor = new VersionExtractor();
        }
        #endregion

        #region When Extracting
        [TestMethod]
        public void ExtractShouldReturnVersion()
        {
            var expectedVersion = Assembly.GetExecutingAssembly().GetName().Version;

            var actualVersion = versionExtractor.Extract();

            Console.WriteLine($"Expected version: {expectedVersion.ToString()}");
            Console.WriteLine($"Actual version: {actualVersion.ToString()}");

            actualVersion.Major.ShouldBeEquivalentTo(expectedVersion.Major);
            actualVersion.Minor.ShouldBeEquivalentTo(expectedVersion.Minor);
            actualVersion.Build.Should().NotBe(default(Int32));
            actualVersion.Revision.Should().NotBe(default(Int32));
        }
        #endregion
    }
}
