﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Orangebox.Common.Xaml.Wpf.Mvvm.Navigation;
using System;
using System.Windows.Controls;

namespace Orangebox.Common.Tests.Xaml.Wpf.Mvvm.Navigation
{
    [TestClass]
    public class FrameNavigationServiceTests
    {
        //private readonly MockFrame TEST_FRAME = new Frame() as MockFrame;
        private MockFrame TEST_FRAME;
        private Func<Frame> frameGetter;
        private FrameNavigationService frameNavigationService;

        #region Setup
        [ClassInitialize]
        public static void BeforeAll(TestContext tc)
        {

        }

        [TestInitialize]
        public void BeforeEach()
        {
            var window = new System.Windows.Window();
            TEST_FRAME = Substitute.For<MockFrame>();
            window.Content = TEST_FRAME;
            frameGetter = new Func<Frame>(() => TEST_FRAME);
            frameNavigationService = new FrameNavigationService(frameGetter);
        }
        #endregion

        #region Private methods
        private void ConfigurePages()
        {
            frameNavigationService.ConfigurePage(typeof(Page1), new Page1().Path);
            frameNavigationService.ConfigurePage(typeof(Page2), new Page2().Path);
            frameNavigationService.ConfigurePage(typeof(Page3), new Page3().Path);
            frameNavigationService.ConfigurePage(typeof(Page4), new Page4().Path);
        }
        #endregion

        #region When Initializing
        [TestMethod]
        public void CtorShouldntSetParameter()
        {
            frameNavigationService.PageParameter.Should().BeNull();
        }
        #endregion

        #region When Configuring
        [TestMethod]
        public void ConfigureShouldWorkWithValidArgs()
        {
            var configAction = new Action(() =>
            {
                ConfigurePages();
            });

            configAction.ShouldNotThrow();
        }

        [TestMethod]
        public void ConfigureShouldNotInsertDuplicateKeys()
        {
            var configAction = new Action(() =>
            {
                frameNavigationService.ConfigurePage(typeof(Page1), new Page1().Path);
                frameNavigationService.ConfigurePage(typeof(Page1), new Page2().Path);
            });

            configAction.ShouldThrow<ArgumentException>();
        }

        [TestMethod]
        public void ConfigureShouldNotInsertDuplicateValues()
        {
            var configAction = new Action(() =>
            {
                frameNavigationService.ConfigurePage(typeof(Page1), new Page1().Path);
                frameNavigationService.ConfigurePage(typeof(Page2), new Page1().Path);
            });

            configAction.ShouldThrow<ArgumentException>();
        }
        #endregion

        #region When Navigating
        [TestMethod]
        public void NavigateToShouldNotSetUnspecifiedParameter()
        {
            ConfigurePages();

            frameNavigationService.NavigateTo(typeof(Page1));

            frameNavigationService.PageParameter.Should().BeNull();
        }

        [TestMethod]
        public void NavigateToShouldSetSpecifiedParameter()
        {
            var parameter = 123;
            ConfigurePages();

            frameNavigationService.NavigateTo(typeof(Page1), parameter);

            frameNavigationService.PageParameter.ShouldBeEquivalentTo(parameter);
        }

        [TestMethod]
        public void NavigateToShouldUpdateCurrentPageKey()
        {
            ConfigurePages();
            frameNavigationService.MonitorEvents();

            frameNavigationService.NavigateTo(typeof(Page1));

            frameNavigationService.CurrentPageKey.ShouldBeEquivalentTo(typeof(Page1).Name);
            frameNavigationService.ShouldRaise("PropertyChanged");
        }

        [TestMethod]
        public void NavigateToShouldChangeCurrentView()
        {
            ConfigurePages();
            frameNavigationService.NavigateTo(typeof(Page1));
            TEST_FRAME.Source.ShouldBeEquivalentTo((new Page1()).Path);
        }

        [TestMethod]
        public void NavigateToShouldNotHandleUnregisteredKeys()
        {
            var navAction = new Action(() =>
            {
                frameNavigationService.NavigateTo(DateTime.Now.Ticks.ToString());
            });

            navAction.ShouldThrow<ArgumentException>();
        }
        #endregion

        #region When Going Forward
        [TestMethod]
        public void CanGoForwardShouldReturnFrameDotCanGoForward()
        {
            var testFrameCanGoForward = TEST_FRAME.NavigationService.CanGoForward;
            var navServiceCanGoForward = frameNavigationService.CanGoForward();
            navServiceCanGoForward.ShouldBeEquivalentTo(testFrameCanGoForward);
        }

        /*
         * CanGoForward() can't be tested - the underlying CanGoForward() always returns false when unit testing
         */
        //[TestMethod]
        //public void CanGoForwardShouldReturnTrueOnNonEmptyForwardStack()
        //{
        //    ConfigurePages();
        //    frameNavigationService.NavigateTo(typeof(Page1));
        //    frameNavigationService.NavigateTo(typeof(Page2));
        //    frameNavigationService.GoBack();
        //    frameNavigationService.CanGoForward().Should().BeTrue();
        //}

        /*
         * GoForward() can't be tested - the underlying CanGoForward() always returns false when unit testing
         */
        //[TestMethod]
        //public void GoForwardShouldUpdateCurrentPageKey()
        //{
        //    ConfigurePages();
        //    frameNavigationService.NavigateTo(typeof(Page1));
        //    frameNavigationService.NavigateTo(typeof(Page2));
        //    frameNavigationService.GoBack();
        //    frameNavigationService.MonitorEvents();
        //
        //    if (!frameNavigationService.CanGoForward())
        //        throw new Exception("You should be able to go forward!");
        //
        //    frameNavigationService.GoForward();
        //
        //    frameNavigationService.CurrentPageKey.ShouldBeEquivalentTo(typeof(Page2).Name);
        //    frameNavigationService.ShouldRaise("PropertyChanged");
        //}
        #endregion

        #region When Going Backward
        [TestMethod]
        public void CanGoBackShouldReturnFrameDotCanGoBack()
        {
            var testFrameCanGoBack = TEST_FRAME.NavigationService.CanGoBack;
            var navServiceCanGoBack = frameNavigationService.CanGoBack();
            navServiceCanGoBack.ShouldBeEquivalentTo(testFrameCanGoBack);
        }

        /*
         * CanGoBack() can't be tested - the underlying CanGoBack() always returns false when unit testing
         */
        //[TestMethod]
        //public void CanGoBackShouldReturnTrueOnNonEmptyBackStack()
        //{
        //    ConfigurePages();
        //    frameNavigationService.NavigateTo(typeof(Page1));
        //    frameNavigationService.NavigateTo(typeof(Page2));
        //    TEST_FRAME.BackStack.Should().NotBeNull();
        //    TEST_FRAME.CanGoBack.Should().BeTrue();
        //    frameNavigationService.CanGoBack().Should().BeTrue();
        //
        //    frameNavigationService.GoBack();
        //    TEST_FRAME.Received().GoBack();
        //}

        /*
         * GoBack() can't be tested - the underlying CanGoBack() always returns false when unit testing
         */
        //[TestMethod]
        //public void GoBackShouldUpdateCurrentPageKey()
        //{
        //    ConfigurePages();
        //    frameNavigationService.NavigateTo(typeof(Page1));
        //    frameNavigationService.NavigateTo(typeof(Page2));
        //    frameNavigationService.MonitorEvents();
        //
        //    if (!frameNavigationService.CanGoBack())
        //        throw new Exception("You should be able to go back!");
        //
        //    frameNavigationService.GoBack();
        //
        //    frameNavigationService.CurrentPageKey.ShouldBeEquivalentTo(typeof(Page1).Name);
        //    frameNavigationService.ShouldRaise("PropertyChanged");
        //}
        #endregion
    }

    public abstract class MockFrame : Frame
    {
        public MockFrame()
        {
            JournalOwnership = System.Windows.Navigation.JournalOwnership.OwnsJournal;
        }
    }

    internal abstract class PageBase : Page
    {
        public string Path {  get { return $"Pages/{GetType().Name}.xaml"; } }
    }

    internal class Page1 : PageBase { }
    internal class Page2 : PageBase { }
    internal class Page3 : PageBase { }
    internal class Page4 : PageBase { }
}
