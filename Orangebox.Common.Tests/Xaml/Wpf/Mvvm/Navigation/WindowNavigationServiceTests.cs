﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orangebox.Common.Xaml.Wpf.Mvvm.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Orangebox.Common.Tests.Xaml.Wpf.Mvvm.Navigation
{
    [TestClass]
    public class WindowNavigationServiceTests
    {
        private WindowNavigationService windowNavigationService;

        #region Setup
        [ClassInitialize]
        public static void BeforeAll(TestContext tc)
        {

        }

        [TestInitialize]
        public void BeforeEach()
        {
            var mainWindowGetter = new Func<Window>(() => new Window());
            windowNavigationService = new WindowNavigationService(null);
        }
        #endregion

        #region When Initializing
        [TestMethod]
        public void CtorShouldntSetParameter()
        {
            windowNavigationService.WindowParameter.Should().BeNull();
        }
        #endregion

        #region When Configuring (Generic)
        [TestMethod]
        public void GenericConfigureShouldWorkWithValidArgs()
        {
            var configAction = new Action(() =>
            {
                windowNavigationService.ConfigureWindow<Window1>(typeof(Window1).Name);
                windowNavigationService.ConfigureWindow<Window2>(typeof(Window2).Name);
            });

            configAction.ShouldNotThrow();
        }

        [TestMethod]
        public void GenericConfigureShouldNotInsertDuplicateKeys()
        {
            var configAction = new Action(() =>
            {
                windowNavigationService.ConfigureWindow<Window1>(typeof(Window1).Name);
                windowNavigationService.ConfigureWindow<Window2>(typeof(Window1).Name);
            });

            configAction.ShouldThrow<ArgumentException>();
        }

        [TestMethod]
        public void GenericConfigureShouldNotInsertDuplicateValues()
        {
            var configAction = new Action(() =>
            {
                windowNavigationService.ConfigureWindow<Window1>(typeof(Window1).Name);
                windowNavigationService.ConfigureWindow<Window1>(typeof(Window1).Name + "-1");
            });

            configAction.ShouldThrow<ArgumentException>();
        }
        #endregion

        #region When Configuring (Explicit Parameter)
        [TestMethod]
        public void ConfigureShouldWorkWithValidArgs()
        {
            var configAction = new Action(() =>
            {
                windowNavigationService.ConfigureWindow(typeof(Window1), typeof(Window1).Name);
                windowNavigationService.ConfigureWindow(typeof(Window2), typeof(Window2).Name);
            });

            configAction.ShouldNotThrow();
        }

        [TestMethod]
        public void ConfigureShouldNotInsertDuplicateKeys()
        {
            var configAction = new Action(() =>
            {
                windowNavigationService.ConfigureWindow(typeof(Window1), typeof(Window1).Name);
                windowNavigationService.ConfigureWindow(typeof(Window2), typeof(Window1).Name);
            });

            configAction.ShouldThrow<ArgumentException>();
        }

        [TestMethod]
        public void ConfigureShouldNotInsertDuplicateValues()
        {
            var configAction = new Action(() =>
            {
                windowNavigationService.ConfigureWindow(typeof(Window1), typeof(Window1).Name);
                windowNavigationService.ConfigureWindow(typeof(Window1), typeof(Window1).Name + "-1");
            });

            configAction.ShouldThrow<ArgumentException>();
        }
        #endregion

        #region When Opening Window
        [TestMethod]
        public void OpenWindowShouldNotSetUnspecifiedParameter()
        {
            windowNavigationService.ConfigureWindow<Window1>(typeof(Window1).Name);

            windowNavigationService.OpenWindow(typeof(Window1).Name, false, false);

            windowNavigationService.WindowParameter.Should().BeNull();
        }

        [TestMethod]
        public void OpenWindowShouldSetSpecifiedParameter()
        {
            var parameter = 123;
            windowNavigationService.ConfigureWindow<Window1>(typeof(Window1).Name);

            windowNavigationService.OpenWindow(typeof(Window1).Name, parameter, false, false);

            windowNavigationService.WindowParameter.ShouldBeEquivalentTo(parameter);
        }
        #endregion
    }

    internal class Window1 : Window { }
    internal class Window2 : Window { }
}
