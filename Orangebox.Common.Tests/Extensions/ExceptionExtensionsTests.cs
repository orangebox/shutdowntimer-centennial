﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orangebox.Common.Extensions;
using System;

namespace Orangebox.Common.Tests.Extensions
{
    [TestClass]
    public class ExceptionExtensionsTests
    {
        #region Private Methods
        private Exception GetSingleException()
        {
            try { throw new Exception("This is a sample exception"); }
            catch (Exception ex) { return ex; }
        }

        private Exception GetNestedException(int count)
        {
            Exception previousException = null;
            Exception currentException = null;

            for (var index = 0; index < count; index++)
            {
                try
                {
                    throw new Exception($"Exception {index}", previousException);
                }
                catch (Exception ex)
                {
                    currentException = ex;
                    previousException = currentException;
                }
            }

            return currentException;
        }
        #endregion

        #region When Flattening
        [TestMethod]
        public void FlattenShouldHandleSingleException()
        {
            var exceptionList = GetSingleException().Flatten();

            exceptionList.Should().HaveCount(1);
        }

        [TestMethod]
        public void FlattenShouldHandleNullException()
        {
            Exception nullException = null;

            var exceptionList = nullException.Flatten();

            exceptionList.Should().BeEmpty();
        }

        [TestMethod]
        public void FlattenShouldHandleNestedExceptions()
        {
            var exceptionCount = 5;
            var nestedExceptions = GetNestedException(exceptionCount);

            var exceptionList = nestedExceptions.Flatten();

            exceptionList.Should().HaveCount(exceptionCount);
        }
        #endregion
    }
}
