﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orangebox.Common.Extensions;
using System;

namespace Orangebox.Common.Tests.Extensions
{
    [TestClass]
    public class DateTimeExtensionsTests
    {
        private DateTime dateTime;

        #region Setup
        [TestInitialize]
        public void BeforeEach()
        {
            dateTime = new DateTime(1989, 2, 10, 1, 2, 3, 4);
        }
        #endregion

        #region Trim
        [TestMethod]
        public void TrimShouldDoNothingWithTicksPerMillisecond()
        {
            var trimmedDateTime = dateTime.Trim(TimeSpan.TicksPerMillisecond);

            trimmedDateTime.ShouldBeEquivalentTo(dateTime);
        }

        [TestMethod]
        public void TrimShouldSetMillisecondsToZeroWithTicksPerSecond()
        {
            var trimmedDateTime = dateTime.Trim(TimeSpan.TicksPerSecond);

            trimmedDateTime.Millisecond.ShouldBeEquivalentTo(0);

            trimmedDateTime.Second.ShouldBeEquivalentTo(dateTime.Second);
            trimmedDateTime.Minute.ShouldBeEquivalentTo(dateTime.Minute);
            trimmedDateTime.Hour.ShouldBeEquivalentTo(dateTime.Hour);
            trimmedDateTime.Day.ShouldBeEquivalentTo(dateTime.Day);
        }

        [TestMethod]
        public void TrimShouldSetSecondsToZeroWithTicksPerMinute()
        {
            var trimmedDateTime = dateTime.Trim(TimeSpan.TicksPerMinute);

            trimmedDateTime.Millisecond.ShouldBeEquivalentTo(0);
            trimmedDateTime.Second.ShouldBeEquivalentTo(0);

            trimmedDateTime.Minute.ShouldBeEquivalentTo(dateTime.Minute);
            trimmedDateTime.Hour.ShouldBeEquivalentTo(dateTime.Hour);
            trimmedDateTime.Day.ShouldBeEquivalentTo(dateTime.Day);
        }

        [TestMethod]
        public void TrimShouldSetMinutesToZeroWithTicksPerHour()
        {
            var trimmedDateTime = dateTime.Trim(TimeSpan.TicksPerHour);

            trimmedDateTime.Millisecond.ShouldBeEquivalentTo(0);
            trimmedDateTime.Second.ShouldBeEquivalentTo(0);
            trimmedDateTime.Minute.ShouldBeEquivalentTo(0);

            trimmedDateTime.Hour.ShouldBeEquivalentTo(dateTime.Hour);
            trimmedDateTime.Day.ShouldBeEquivalentTo(dateTime.Day);
        }

        [TestMethod]
        public void TrimShouldSetHoursToZeroWithTicksPerDay()
        {
            var trimmedDateTime = dateTime.Trim(TimeSpan.TicksPerDay);

            trimmedDateTime.Millisecond.ShouldBeEquivalentTo(0);
            trimmedDateTime.Second.ShouldBeEquivalentTo(0);
            trimmedDateTime.Minute.ShouldBeEquivalentTo(0);
            trimmedDateTime.Hour.ShouldBeEquivalentTo(0);

            trimmedDateTime.Day.ShouldBeEquivalentTo(dateTime.Day);
        }
        #endregion
    }
}
