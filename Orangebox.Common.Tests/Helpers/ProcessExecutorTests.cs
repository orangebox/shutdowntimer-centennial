﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Orangebox.Common.Helpers;
using Orangebox.Common.Logging;
using System;

namespace Orangebox.Common.Tests.Helpers
{
    [TestClass]
    public class ProcessExecutorTests
    {
        #region Variables
        private ILogger logger;
        private ProcessExecutor processExecutor;
        #endregion

        #region Setup
        [TestInitialize]
        public void BeforeEach()
        {
            logger = Substitute.For<ILogger>();
            processExecutor = new ProcessExecutor(logger);
        }
        #endregion

        #region When Initializing
        [TestMethod]
        public void CtorShouldInitializeLogger()
        {
            logger.Received().Initialize(processExecutor.GetType());
        }
        #endregion

        #region When Running
        [TestMethod]
        public void RunShouldReturnExitCode()
        {
            var exitCode = processExecutor.Run("ipconfig", "/all");

            exitCode.ShouldBeEquivalentTo(0);
        }

        [TestMethod]
        public void StandardOutputShouldBeLogged()
        {
            processExecutor.Run("ipconfig", "/all");

            logger.Received().Info(Arg.Any<String>());
        }

        [TestMethod]
        public void RunShouldHandleEmptyParameters()
        {
            var exitCode = processExecutor.Run("ipconfig", String.Empty);

            exitCode.ShouldBeEquivalentTo(0);
        }
        #endregion
    }
}
