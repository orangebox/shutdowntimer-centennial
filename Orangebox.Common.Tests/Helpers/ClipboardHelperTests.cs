﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orangebox.Common.Helpers;
using System;
using System.Windows;

namespace Orangebox.Common.Tests.Helpers
{
    [TestClass]
    public class ClipboardHelperTests
    {
        #region Variables
        private ClipboardHelper clipboardHelper;
        #endregion

        #region Setup
        [TestInitialize]
        public void BeforeEach()
        {
            clipboardHelper = new ClipboardHelper();
        }
        #endregion

        #region When Copying
        [TestMethod]
        public void CopyShouldSetTextToClipboard()
        {
            var text = DateTime.Now.Ticks.ToString("N0");

            clipboardHelper.Copy(text);

            Clipboard.GetText().ShouldBeEquivalentTo(text);
        }
        #endregion
    }
}
