﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Orangebox.Common.Logging;
using Orangebox.Common.Xaml.Wpf.Mvvm.Dialog;
using Orangebox.Common.Xaml.Wpf.Mvvm.Navigation;
using Orangebox.ShutdownTimer.BusinessLayer.Logic;
using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;

namespace Orangebox.ShutdownTimer.Backend.ViewModels
{
    public class TimerViewModel : ViewModelBase
    {
        #region Variables
        private readonly IFrameNavigationService navService;
        private readonly IDialogService dialogService;
        private readonly ICountdownExecutor countdownExecutor;
        private readonly IPowerActionExecutor powerActionExecutor;
        private readonly ILogger logger;
        #endregion

        #region Binding Variables
        private ScheduledPowerAction scheduledPowerAction;
        private TimeSpan remainingTime;
        private int initialDays;
        private bool isDaysVisible;
        private bool isHoursVisible;
        private bool isMinutesVisible;

        public ScheduledPowerAction ScheduledPowerAction
        {
            get { return scheduledPowerAction; }
            set { Set(nameof(ScheduledPowerAction), ref scheduledPowerAction, value); }
        }
        public TimeSpan RemainingTime
        {
            get { return remainingTime; }
            set { Set(nameof(remainingTime), ref remainingTime, value); }
        }
        public int InitialDays
        {
            get { return initialDays; }
            set { Set(nameof(InitialDays), ref initialDays, value); }
        }
        public bool IsDaysVisible
        {
            get { return isDaysVisible; }
            set { Set(nameof(IsDaysVisible), ref isDaysVisible, value); }
        }
        public bool IsHoursVisible
        {
            get { return isHoursVisible; }
            set { Set(nameof(IsHoursVisible), ref isHoursVisible, value); }
        }
        public bool IsMinutesVisible
        {
            get { return isMinutesVisible; }
            set { Set(nameof(IsMinutesVisible), ref isMinutesVisible, value); }
        }
        #endregion

        #region Commands
        public RelayCommand LoadedCommand { get; private set; }
        public RelayCommand AbortCommand { get; private set; }
        #endregion

        #region Constructors
        public TimerViewModel(IFrameNavigationService navSvc,
                              IDialogService dlgSvc,
                              ICountdownExecutor countExec,
                              IPowerActionExecutor powActExec,
                              ILogger logr)
        {
            navService = navSvc;
            dialogService = dlgSvc;
            countdownExecutor = countExec;
            powerActionExecutor = powActExec;
            logger = logr;

            LoadedCommand = new RelayCommand(LoadedExecuted);
            AbortCommand = new RelayCommand(AbortExecuted, AbortCanExecute);
        }
        #endregion

        #region Private methods
        private void UpdateIsRunning(bool value)
        {
            countdownExecutor.IsRunning = value;
            AbortCommand.RaiseCanExecuteChanged();
        }

        private async void RunTimerAsync()
        {
            var countdownResult = await countdownExecutor.RunTimer(ScheduledPowerAction.TimeOfAction, (rt) => RemainingTime = rt);
            if (countdownResult.IsInterrupted)
            {
                logger.Trace("Timer interrupted by user");
                return;
            }

            var powerActionResult = powerActionExecutor.Run(ScheduledPowerAction.PowerAction);
            if(!powerActionResult.IsSuccessful)
            {
                dialogService.ShowMessage($"The power action failed because:\n\n{powerActionResult.ErrorMessage}", "Error");
                logger.Error($"Power action failed with exit code {powerActionResult.ExitCode} - {powerActionResult.ErrorMessage}");
            }
            navService.NavigateTo(typeof(ParamsViewModel));
        }

        private void SetVisibilities()
        {
#if DEBUG
            var previousIsDaysVisible = IsDaysVisible;
            var previousIsHoursVisible = IsHoursVisible;
            var previousIsMinutesVisible = IsMinutesVisible;
#endif

            IsDaysVisible = RemainingTime.Days > 0;
            IsHoursVisible = IsDaysVisible || RemainingTime.Hours > 0;
            IsMinutesVisible = IsHoursVisible || RemainingTime.Minutes > 0;

#if DEBUG
            if (previousIsDaysVisible != IsDaysVisible)
                logger.Info($"IsDaysVisible is now {IsDaysVisible}");
            if (previousIsHoursVisible != IsHoursVisible)
                logger.Info($"IsHoursVisible is now {IsHoursVisible}");
            if (previousIsMinutesVisible != IsMinutesVisible)
                logger.Info($"IsMinutesVisible is now {IsMinutesVisible}");
#endif
        }
        #endregion

        #region Commands CanExecute
        private bool AbortCanExecute()
        {
            return countdownExecutor.IsRunning;
        }
        #endregion

        #region Commands Executed
        private void LoadedExecuted()
        {
            logger.Initialize(GetType());
            ScheduledPowerAction = navService.PageParameter as ScheduledPowerAction;
            RemainingTime = ScheduledPowerAction.TimeOfAction - DateTime.Now;
            InitialDays = RemainingTime.Days;
            SetVisibilities();

            RunTimerAsync();
        }

        private void AbortExecuted()
        {
            logger.Info($"Aborting {ScheduledPowerAction.PowerAction.Name}");
            UpdateIsRunning(false);
            navService.GoBack();
        }
        #endregion
    }
}
