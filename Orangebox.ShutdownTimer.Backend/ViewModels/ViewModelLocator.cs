﻿using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using Orangebox.Common.Helpers;
using Orangebox.Common.Logging;
using Orangebox.Common.Xaml.Wpf.Mvvm.Dialog;
using Orangebox.Common.Xaml.Wpf.Mvvm.Navigation;
using Orangebox.ShutdownTimer.Backend.Helpers;
using Orangebox.ShutdownTimer.BusinessLayer.Logic;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Orangebox.ShutdownTimer.Backend.ViewModels
{
    public class ViewModelLocator
    {
        #region View Models
        public SplashViewModel Splash { get { return ServiceLocator.Current.GetInstance<SplashViewModel>(); } }
        public MainViewModel Main { get { return ServiceLocator.Current.GetInstance<MainViewModel>(); } }
        public ParamsViewModel Params { get { return ServiceLocator.Current.GetInstance<ParamsViewModel>(); } }
        public TimerViewModel Timer { get { return ServiceLocator.Current.GetInstance<TimerViewModel>(); } }
        public ExceptionViewModel Exception { get { return ServiceLocator.Current.GetInstance<ExceptionViewModel>(); } }
        #endregion

        #region Services
        public IWindowNavigationService WindowNavigationService { get { return ServiceLocator.Current.GetInstance<IWindowNavigationService>(); } }
        public IFrameNavigationService FrameNavigationService { get { return ServiceLocator.Current.GetInstance<IFrameNavigationService>(); } }
        public ISpaValidator SpaValidator { get { return ServiceLocator.Current.GetInstance<SpaValidator>(); } }
        public ICountdownExecutor CountdownExecutor { get { return ServiceLocator.Current.GetInstance<ICountdownExecutor>(); } }
        #endregion

        #region Constructors
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
        }
        #endregion

        #region Private methods
        private IFrameNavigationService CreateFrameNavService(Func<Frame> frameGetter)
        {
            var fns = new FrameNavigationService(frameGetter);

            fns.ConfigurePage(typeof(SplashViewModel), "Views/SplashView.xaml");
            fns.ConfigurePage(typeof(ParamsViewModel), "Views/ParamsView.xaml");
            fns.ConfigurePage(typeof(TimerViewModel), "Views/TimerView.xaml");

            return fns;
        }

        private IWindowNavigationService CreateWindowNavService(Func<Window> windowGetter, params Type[] windows)
        {
            var wns = new WindowNavigationService(windowGetter);

            foreach(var windowType in windows)
            {
                wns.ConfigureWindow(windowType, windowType.Name);
            }

            return wns;
        }
        #endregion

        #region Public Methods
        public void InitializeServices(Func<Frame> frameGetter, Func<Window> windowGetter, params Type[] windows)
        {
            SimpleIoc.Default.Register<IDialogService, DialogService>();
            SimpleIoc.Default.Register<IFrameNavigationService>(() => CreateFrameNavService(frameGetter));
            SimpleIoc.Default.Register<IWindowNavigationService>(() => CreateWindowNavService(windowGetter, windows));
            SimpleIoc.Default.Register<ISpaValidator, SpaValidator>();
            SimpleIoc.Default.Register<IPowerActionsRetriever, PowerActionsRetriever>();
            SimpleIoc.Default.Register<ICountdownExecutor, CountdownExecutor>();
            SimpleIoc.Default.Register<IVersionExtractor, VersionExtractor>();
            SimpleIoc.Default.Register<ILogger, NLogLogger>();
            SimpleIoc.Default.Register<IScheduleParameterAppender, ScheduleParameterAppender>();
            SimpleIoc.Default.Register<ICloseHandler, CloseHandler>();

            SimpleIoc.Default.Register<IClipboardHelper, ClipboardHelper>();
            SimpleIoc.Default.Register<IExceptionCopier, ExceptionCopier>();

            SimpleIoc.Default.Register<IProcessExecutor, ProcessExecutor>();
#if DEBUG
            SimpleIoc.Default.Register<IPowerActionExecutor, DebugPowerActionExecutor>();
#else
            SimpleIoc.Default.Register<IPowerActionExecutor, PowerActionExecutor>();
#endif
        }

        public void InitializeViewModels()
        {
            SimpleIoc.Default.Register<SplashViewModel>();
            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<ParamsViewModel>();
            SimpleIoc.Default.Register<TimerViewModel>();
            SimpleIoc.Default.Register<ExceptionViewModel>();
        }
        #endregion
    }
}
