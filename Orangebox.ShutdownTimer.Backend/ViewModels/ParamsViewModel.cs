﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Orangebox.Common.Logging;
using Orangebox.Common.Xaml.Wpf.Mvvm.Dialog;
using Orangebox.Common.Xaml.Wpf.Mvvm.Navigation;
using Orangebox.ShutdownTimer.Backend.Helpers;
using Orangebox.ShutdownTimer.BusinessLayer.Logic;
using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Orangebox.ShutdownTimer.Backend.ViewModels
{
    public class ParamsViewModel : ViewModelBase
    {
        #region Variables
        private readonly IDialogService dialogService;
        private readonly IFrameNavigationService navService;
        private readonly IPowerActionsRetriever powerActionsRetriever;
        private readonly ISpaValidator spaValidator;
        private readonly IVersionExtractor versionExtractor;
        private readonly ILogger logger;
        #endregion

        #region Binding Variables
        private List<PowerAction> powerActions;
        private PowerAction selectedPowerAction;
        private DateTime selectedDateTime;

        public List<PowerAction> PowerActions
        {
            get { return powerActions; }
            set { Set(nameof(PowerActions), ref powerActions, value); }
        }
        public PowerAction SelectedPowerAction
        {
            get { return selectedPowerAction; }
            set { Set(nameof(SelectedPowerAction), ref selectedPowerAction, value); }
        }
        public DateTime SelectedDateTime
        {
            get { return selectedDateTime; }
            set { Set(nameof(SelectedDateTime), ref selectedDateTime, value); }
        }
        #endregion

        #region Commands
        public RelayCommand LoadedCommand { get; private set; }
        public RelayCommand SubmitCommand { get; private set; }
        public RelayCommand AboutCommand { get; private set; }
        #endregion

        #region Constructors
        public ParamsViewModel(IFrameNavigationService navSvc,
                               IPowerActionsRetriever par,
                               ISpaValidator sv,
                               IDialogService dlg,
                               IVersionExtractor verExt,
                               ILogger logr)
        {
            dialogService = dlg;
            navService = navSvc;
            powerActionsRetriever = par;
            spaValidator = sv;
            versionExtractor = verExt;
            logger = logr;

            LoadedCommand = new RelayCommand(LoadedExecuted);
            SubmitCommand = new RelayCommand(SubmitExecuted, SubmitCanExecute);
            AboutCommand = new RelayCommand(AboutExecuted);
        }
        #endregion

        #region Private methods
        #endregion

        #region Commands CanExecute
        private bool SubmitCanExecute()
        {
            return SelectedPowerAction != null &&
                   SelectedDateTime != default(DateTime);
        }
        #endregion

        #region Commands Executed
        private void LoadedExecuted()
        {
            logger.Initialize(GetType());
            PowerActions = new List<PowerAction>(powerActionsRetriever.GetPowerActions());
            SelectedPowerAction = PowerActions.First();
            SelectedDateTime = DateTime.Now;
        }

        private void SubmitExecuted()
        {
            var spa = new ScheduledPowerAction(SelectedPowerAction, SelectedDateTime);
            var validationResult = spaValidator.Validate(spa);
            if (validationResult.IsSuccessful)
            {
                logger.Info($"Submitting action: {spa.PowerAction.Name}, {spa.TimeOfAction}");
                navService.NavigateTo(typeof(TimerViewModel), spa);
            }
            else
                dialogService.ShowMessage(validationResult.ErrorMessage, "Error");
        }

        private void AboutExecuted()
        {
            var version = versionExtractor.Extract();
            dialogService.ShowMessage($"Version: {version.FullVersion}", "About ShutdownTimer");
        }
        #endregion
    }
}
