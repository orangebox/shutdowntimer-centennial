﻿using System.Windows.Input;

namespace Orangebox.ShutdownTimer.Backend.ViewModels.Design
{
    public class SplashViewModel
    {
        public string Version { get { return "v.2.0.1234.56789"; } }

        public ICommand LoadedCommand { get; private set; }

        public SplashViewModel()
        {

        }

    }
}
