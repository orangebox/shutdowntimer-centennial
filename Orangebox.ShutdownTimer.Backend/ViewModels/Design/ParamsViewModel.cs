﻿using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace Orangebox.ShutdownTimer.Backend.ViewModels.Design
{
    public class ParamsViewModel
    {
        public List<PowerAction> PowerActions { get; set; }
        public PowerAction SelectedPowerAction { get; set; }
        public DateTime SelectedDateTime { get; set; }

        public ICommand LoadedCommand { get; private set; }
        public ICommand SubmitCommand { get; private set; }
        public ICommand AboutCommand { get; private set; }

        public ParamsViewModel()
        {
            PowerActions = new List<PowerAction>();
            for(var index = 0; index < 30; index++)
            {
                var pa = new PowerAction($"Power Action {index + 1}", 
                                         $"Filename {index + 1}", 
                                         $"Argument {index + 1}", 
                                         $"Description {index + 1}",
                                         false);
                PowerActions.Add(pa);
            }
            SelectedPowerAction = PowerActions.First();
            SelectedDateTime = DateTime.Now;
        }

    }
}
