﻿using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace Orangebox.ShutdownTimer.Backend.ViewModels.Design
{
    public class ExceptionViewModel
    {
        public List<ExceptionModel> Exceptions { get; private set; }

        public ICommand LoadedCommand { get; private set; }
        public ICommand CopySingleCommand { get; private set; }
        public ICommand CopyAllCommand { get; private set; }

        public ExceptionViewModel()
        {
            Exceptions = new List<ExceptionModel>();

            for (var index = 0; index < 20; index++)
            {
                try
                {
                    GenerateException(index);
                }
                catch (Exception ex)
                {
                    Exceptions.Add(new ExceptionModel(ex));
                }
            }
        }

        private void GenerateException(int index)
        {
            var message = $"Sample exception message {index + 1}";
            switch (index % 5)
            {
                case 0:
                    throw new Exception(message);
                case 1:
                    throw new ArgumentException(message);
                case 2:
                    throw new NullReferenceException(message);
                case 3:
                    throw new EntryPointNotFoundException(message);
                default:
                    throw new NotImplementedException(message);
            }
        }

    }
}
