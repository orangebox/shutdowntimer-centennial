﻿using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;
using System.Windows.Input;

namespace Orangebox.ShutdownTimer.Backend.ViewModels.Design
{
    public class TimerViewModel
    {
        public ScheduledPowerAction ScheduledPowerAction { get; set; }
        public TimeSpan RemainingTime { get; set; }
        public int InitialDays { get; set; }
        public bool IsDaysVisible { get; set; }
        public bool IsHoursVisible { get; set; }
        public bool IsMinutesVisible { get; set; }

        public ICommand LoadedCommand { get; private set; }
        public ICommand AbortCommand { get; private set; }

        public TimerViewModel()
        {
            var pa = new PowerAction("Sample PA Name", 
                                     "Sample PA Filename", 
                                     "Sample PA Argument", 
                                     "Sample PA Description",
                                     false);
            var dt = new DateTime(1987, 7, 16, 0, 30, 0);
            ScheduledPowerAction = new ScheduledPowerAction(pa, dt);

            RemainingTime = new TimeSpan(24, 23, 59, 59);
            InitialDays = RemainingTime.Days + 1;
            IsDaysVisible = true;
            IsHoursVisible = true;
            IsMinutesVisible = true;
        }

    }
}
