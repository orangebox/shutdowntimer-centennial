﻿using System.Windows.Input;

namespace Orangebox.ShutdownTimer.Backend.ViewModels.Design
{
    public class MainViewModel
    {
        public string Title { get; private set; }

        public ICommand LoadedCommand { get; private set; }

        public MainViewModel()
        {
            Title = "ShutdownTimer v.123.456.789.012";
        }
    }
}
