﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Orangebox.Common.Logging;
using Orangebox.Common.Xaml.Wpf.Mvvm.Navigation;
using Orangebox.ShutdownTimer.Backend.Helpers;
using Orangebox.ShutdownTimer.BusinessLayer.Logic;
using Orangebox.ShutdownTimer.BusinessLayer.Models;

namespace Orangebox.ShutdownTimer.Backend.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        #region Variables
        private readonly IFrameNavigationService navService;
        private readonly IVersionExtractor versionExtractor;
        private readonly ILogger logger;
        private readonly ICountdownExecutor countdownExecutor;
        private readonly ICloseHandler closeHandler;
        #endregion

        #region Binding Variables
        private string title;

        public string Title
        {
            get { return title; }
            set { Set(nameof(Title), ref title, value); }
        }
        #endregion

        #region Commands
        public RelayCommand LoadedCommand { get; private set; }
        #endregion

        #region Constructors
        public MainViewModel(IFrameNavigationService navSvc, 
                             IVersionExtractor verExt, 
                             ILogger logr, 
                             ICountdownExecutor countExec,
                             ICloseHandler clsHndlr)
        {
            navService = navSvc;
            versionExtractor = verExt;
            logger = logr;
            countdownExecutor = countExec;
            closeHandler = clsHndlr;

            LoadedCommand = new RelayCommand(LoadedExecuted);
        }
        #endregion

        #region Commands Executed
        private void LoadedExecuted()
        {
            var version = versionExtractor.Extract();
            Title = $"ShutdownTimer v.{version.ToString()}";

            logger.Initialize(GetType());
            logger.Info(Title);

            navService.NavigateTo(typeof(SplashViewModel));
        }
        #endregion

        public bool CheckCanClose()
        {
            var isCountdownRunning = countdownExecutor.IsRunning;
            var spa = navService.PageParameter as ScheduledPowerAction;
            return closeHandler.CheckCanClose(isCountdownRunning, spa);
        }

    }
}
