﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Orangebox.Common.Logging;
using Orangebox.Common.Xaml.Wpf.Mvvm.Navigation;
using Orangebox.ShutdownTimer.Backend.Helpers;
using Orangebox.ShutdownTimer.BusinessLayer.Logic;
using System.Threading.Tasks;

namespace Orangebox.ShutdownTimer.Backend.ViewModels
{
    public class SplashViewModel : ViewModelBase
    {
        #region Variables
        private readonly IFrameNavigationService navService;
        private readonly IPowerActionsRetriever powerActionsRetriever;
        private readonly IPowerActionExecutor powerActionExecutor;
        private readonly ILogger logger;
        private readonly IVersionExtractor versionExtractor;
        #endregion

        #region Binding Variables
        private string version;
        public string Version
        {
            get { return version; }
            set { Set(nameof(Version), ref version, value); }
        }
        #endregion

        #region Commands
        public RelayCommand LoadedCommand { get; private set; }
        #endregion

        #region Constructors
        public SplashViewModel(IFrameNavigationService fns,
                               IPowerActionsRetriever pwrActRetriever,
                               IPowerActionExecutor powExec,
                               ILogger logr,
                               IVersionExtractor verExt)
        {
            navService = fns;
            powerActionsRetriever = pwrActRetriever;
            powerActionExecutor = powExec;
            logger = logr;
            versionExtractor = verExt;

            LoadedCommand = new RelayCommand(LoadedExecuted);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Commands CanExecute
        #endregion

        #region Commands Executed
        private async void LoadedExecuted()
        {
            Version = $"v.{versionExtractor.Extract().ToString()}";

            logger.Initialize(GetType());
            logger.Trace("Aborting any scheduled power actions");

            var abortAction = powerActionsRetriever.AbortAction;
            powerActionExecutor.Run(abortAction);
            await Task.Delay(2000);
            navService.NavigateTo(typeof(ParamsViewModel));
        }
        #endregion
    }
}
