﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Orangebox.Common.Extensions;
using Orangebox.Common.Logging;
using Orangebox.Common.Xaml.Wpf.Mvvm.Navigation;
using Orangebox.ShutdownTimer.BusinessLayer.Logic;
using Orangebox.ShutdownTimer.BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Orangebox.ShutdownTimer.Backend.ViewModels
{
    public class ExceptionViewModel : ViewModelBase
    {
        #region Variables
        private readonly IWindowNavigationService windowNavService;
        private readonly ILogger logger;
        private readonly IExceptionCopier exceptionCopier;
        #endregion

        #region Binding Variables
        private List<ExceptionModel> exceptions;
        public List<ExceptionModel> Exceptions
        {
            get { return exceptions; }
            set { Set(nameof(Exceptions), ref exceptions, value); }
        }
        #endregion

        #region Commands
        public RelayCommand LoadedCommand { get; private set; }
        public RelayCommand<ExceptionModel> CopySingleCommand { get; private set; }
        public RelayCommand CopyAllCommand { get; private set; }
        #endregion

        #region Constructors
        public ExceptionViewModel(IWindowNavigationService winNavSvc, 
                                  ILogger logr,
                                  IExceptionCopier exCopy)
        {
            windowNavService = winNavSvc;
            logger = logr;
            exceptionCopier = exCopy;

            LoadedCommand = new RelayCommand(LoadedExecuted);
            CopySingleCommand = new RelayCommand<ExceptionModel>(CopySingleExecuted);
            CopyAllCommand = new RelayCommand(CopyAllExecuted);
        }
        #endregion

        #region Private methods
        #endregion

        #region Commands CanExecute
        #endregion

        #region Commands Executed
        private void LoadedExecuted()
        {
            var exception = windowNavService.WindowParameter as Exception;
            this.Exceptions = exception.Flatten().Select(ex => new ExceptionModel(ex)).ToList();

            logger.Initialize(GetType());
            logger.Exception(exception);
        }

        private void CopySingleExecuted(ExceptionModel ex)
        {
            exceptionCopier.CopyException(ex);
        }

        private void CopyAllExecuted()
        {
            exceptionCopier.CopyExceptions(Exceptions);
        }
        #endregion

    }
}
