﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace Orangebox.ShutdownTimer.Backend.Converters
{
    public class BoolGridLengthConverter : MarkupExtension, IValueConverter
    {
        private readonly GridLength VISIBLE_LENGTH = new GridLength(1, GridUnitType.Star);
        private readonly GridLength HIDDEN_LENGTH = new GridLength(0, GridUnitType.Pixel);

        private static BoolGridLengthConverter converter = null;
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (converter == null)
                converter = new BoolGridLengthConverter();
            return converter;
        }

        public BoolGridLengthConverter() { }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var isVisible = (bool)value;
                return isVisible ? VISIBLE_LENGTH : HIDDEN_LENGTH;
            }
            catch
            {
                return VISIBLE_LENGTH;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var gridLength = (GridLength)value;
                return gridLength == VISIBLE_LENGTH;
            }
            catch
            {
                return true;
            }
        }

    }
}
