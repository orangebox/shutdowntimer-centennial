﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace Orangebox.ShutdownTimer.Backend.Converters
{
    public class StringCaseConverter : MarkupExtension, IValueConverter
    {
        private static StringCaseConverter converter = null;
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (converter == null)
                converter = new StringCaseConverter();
            return converter;
        }

        public StringCaseConverter() { }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var str = value?.ToString() ?? String.Empty;
            if (String.IsNullOrEmpty(str)) return String.Empty;

            switch (parameter as String)
            {
                case "l":
                    return str.ToLower();
                case "u":
                    return str.ToUpper();
                default:
                    throw new NotImplementedException($"Unhandled case: {parameter}");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException($"The string {value?.ToString() ?? "EMPTY"} can't be converted back to its original casing");
        }

    }
}
