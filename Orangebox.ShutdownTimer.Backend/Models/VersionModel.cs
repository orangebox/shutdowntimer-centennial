﻿namespace Orangebox.ShutdownTimer.Backend.Models
{
    public class VersionModel
    {
        #region Binding Variables
        public int Major { get; }
        public int Minor { get; }
        public int Build { get; }
        public int Revision { get; }
        public string FullVersion { get { return $"{Major}.{Minor}.{Build}.{Revision}"; } }
        #endregion

        #region Constructors
        public VersionModel(int maj, int min, int bld, int rev)
        {
            Major = maj;
            Minor = min;
            Build = bld;
            Revision = rev;
        }
        #endregion

        #region Public Methods
        public override string ToString()
        {
#if DEBUG
            return $"{Major}.{Minor}.{Build}.{Revision}";
#else
            return $"{Major}.{Minor}";
#endif
        }
        #endregion

    }
}
