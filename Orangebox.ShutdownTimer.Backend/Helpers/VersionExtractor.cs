﻿using Orangebox.ShutdownTimer.Backend.Models;
using System.Reflection;

namespace Orangebox.ShutdownTimer.Backend.Helpers
{
    public interface IVersionExtractor
    {
        VersionModel Extract();
    }

    public class VersionExtractor : IVersionExtractor
    {
        public VersionModel Extract()
        {
            var version = Assembly.GetExecutingAssembly().GetName().Version;

            return new VersionModel(version.Major, version.Minor, version.Build, version.Revision);
        }
    }
}
